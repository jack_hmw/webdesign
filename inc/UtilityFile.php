<?php

class UtilityFile {

    function __construct()
    {
    }

    function createDirectoryIfNotExists($path){
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
    }
}

$UtilityFile = new UtilityFile();