<?php

include('UtilityFile.php');

class TemplateManager {

    public $export = false;
    public $currentLang = null;
    public $imgBasePrefix = './images/';
    public $imgBaseSuffix = '';
    public $imgBasePath = './images/';
    public $imgNameOnly = false;
    public $imgBasePathOptimized = './images/optimized/';
    public $langStrings = null;
    public $linkStrings = null;
    public $imgToBase64 = false;
    public $maxImgSize = 1024*1024*10; //10 MB
    public $maxWidth = false;

    function __construct($lang){
        global $UtilityFile;
        $this->currentLang = $lang;
        $this->__setFromExternalConfig();

        $this->langStrings = include('./texts/texts_'.$lang.'.php');
        $this->linkStrings = include('./texts/links.php');

        $UtilityFile->createDirectoryIfNotExists($this->imgBasePathOptimized);
    }

    function __setFromExternalConfig(){
        global $TemplateManagerCfg;
        if(!isset($TemplateManagerCfg)) return;
        if(isset($TemplateManagerCfg['export'])) $this->export=$TemplateManagerCfg['export'];
        if(isset($TemplateManagerCfg['lang'])) $this->currentLang=$TemplateManagerCfg['lang'];
        if(isset($TemplateManagerCfg['imgToBase64'])) $this->imgToBase64=$TemplateManagerCfg['imgToBase64'];
        if(isset($TemplateManagerCfg['imgNameOnly'])) $this->imgNameOnly=$TemplateManagerCfg['imgNameOnly'];
        if(isset($TemplateManagerCfg['maxWidth'])) $this->maxWidth=$TemplateManagerCfg['maxWidth'];
    }

    function __($label){
        return $this->langStrings[$label];
    }

    function __lk($label){
        return $this->linkStrings[$label];
    }

    function imgSrc($imgname, $imginfo=null){
        if($this->imgNameOnly===true) return $imgname;
        if($this->imgToBase64!==true) return $this->imgBasePrefix . $imgname . $this->imgBaseSuffix;
        return $this->imgBase64($imgname, $imginfo);
    }


    function imgBase64($imgname, $imginfo=null){
        global $UtilityFile;
        if($this->imgNameOnly===true) return $imgname;
        $path = $this->imgBasePath . $imgname;
        //$path = $this->compress_image($imgname, 99, $imginfo);
        if($imginfo['size']>$this->maxImgSize) return $path.'___';
        if($this->imgToBase64!==true) return $path;
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        return 'data:image/' . $type . ';base64,' . base64_encode($data);
    }


    function imgInfo($imgname){
        $path = $this->imgBasePath . $imgname;
        $img = getimagesize($path);
        if($img){
            return array(
                'width'=>$img[0],
                'height'=>$img[1],
                'size'=>filesize($path)/1024,
                'mime'=>$img['mime']
            );
        }
        return null;
    }


    function compress_image1($imgname, $quality, $info=null) {
        $source_url = $this->imgBasePath . $imgname;
        if ($info['mime'] != 'image/jpeg') return $source_url;
//        elseif ($info['mime'] == 'image/gif')
//            $image = imagecreatefromgif($source_url);
//        elseif ($info['mime'] == 'image/png')
//            $image = imagecreatefrompng($source_url);
        $image = imagecreatefromjpeg($source_url);
        $dest_url = $this->imgBasePathOptimized . $imgname;
        imagejpeg($image, $dest_url, $quality);
        return $dest_url;
    }

    function compress_image2($imgname, $quality, $info=null) {
        $source_url = $this->imgBasePath . $imgname;
        if ($info['mime'] != 'image/jpeg') return $source_url;

        $dest_url = $this->imgBasePathOptimized . $imgname;

        $im = new Imagick($source_url);
        $im->optimizeImageLayers();
        $im->setImageCompression(Imagick::COMPRESSION_JPEG);
        $im->setImageCompressionQuality($quality);
        $im->writeImages($dest_url, true);

        return $dest_url;
    }

    function compress_image() {
        $factory = new \ImageOptimizer\OptimizerFactory();
    }
}


$T = new TemplateManager('nl');
//$T->maxImgSize = 100;
function __lk($lb){ global $T; echo $T->__lk($lb); }
function __lkr($lb){ global $T; return $T->__lk($lb); }
function __($lb){ global $T; return $T->__($lb); }
function __e($lb){ global $T; echo ($T->__($lb)); }
function __eU($lb){ global $T; echo (strtoupper($T->__($lb))); }
function __eL($lb){ global $T; echo (strtolower($T->__($lb))); }

function __img($lb){ global $T; echo $T->imgBase64($lb); }

function __imgBoxCss($lb,$lb2=null,$w=null,$h=null){
    global $T;
    if($lb2==null) $lb2 = str_ireplace('.','_',$lb);
    $imginfo = $T->imgInfo($lb);
    if(isset($w)) $imginfo['width']=$w."; "; else $imginfo['width'].="px; ";
    if(isset($h)) $imginfo['height']=$h."; "; else $imginfo['height'].="px; ";
    echo '.imgbox_'.$lb2." { background-image:url('".$T->imgSrc($lb,$imginfo)."'); ".
        "width:".$imginfo['width'].
        "height:".$imginfo['height'].
        " }\n";
}
function __imgBackCss($lb, $lb2=null, $options=array()){
    global $T;
    if($lb2==null) $lb2 = str_ireplace('.','_',$lb);
    $imginfo = $T->imgInfo($lb);
    $cssRule = '.imgback_'.$lb2." { background-image:url('".$T->imgSrc($lb,$imginfo)."'); ";
    if(isset($options['setheight']) && $options['setheight']==true){
        $cssRule.=" height:".$imginfo['height']."px; ";
    }
    echo $cssRule." }\n";
}


function __imgBoxCssResize($lb,$lb2=null,$w=null,$h=null){
    global $T;
    if($lb2==null) $lb2 = str_ireplace('.','_',$lb);
    $imginfo = $T->imgInfo($lb);
    $imginfo = __parseImageSize($imginfo,$w,$h);
    if(isset($w)) $imginfo['width']=$w."px; "; else $imginfo['width'].="px; ";
    if(isset($h)) $imginfo['height']=$h."px; "; else $imginfo['height'].="px; ";
    echo '.imgbox_'.$lb2." { background-image:url('".$T->imgSrc($lb,$imginfo)."'); ".
        "width:".$imginfo['width'].
        "height:".$imginfo['height'].
        " }\n";
}


function __parseImageSize($imginfo, $w=null, $h=null){
    if($w==null) $w=0;
    if($h==null) $h=0;
    if(($w==0 && $h==0) || ($w==$imginfo['width'] && $h==$imginfo['height'])) return $imginfo;
    if($w>0){
        $imginfo['height'] = round(($w/$imginfo['width'])*$imginfo['height']);
        $imginfo['width'] = $w;
        return $imginfo;
    }
    if($h>0){
        $imginfo['width'] = round(($h/$imginfo['height'])*$imginfo['width']);
        $imginfo['height'] = $h;
        return $imginfo;
    }
}


function __imgCssBackground($lb,$setSize=false){
    global $T;
    $imginfo = $T->imgInfo($lb);
    $cssRule = " background-image:url('" . $T->imgSrc($lb,$imginfo) . "'); ";
    if($setSize===true){
        $cssRule .= ' height:'.$imginfo['height'].'px; ';
    }
    //$cssRule.=" height:".$imginfo['height']."px; ";
    echo $cssRule."\n";
}


function __imgTag($lb, $alt='', $w=null, $h=null){
    global $T;
    $imginfo = $T->imgInfo($lb);
    $imginfo = __parseImageSize($imginfo,$w,$h);
    $cssRule = '<img src="'.$T->imgSrc($lb,$imginfo).'" alt="'.$alt.'" ';
    if(isset($w)) $cssRule .= ' width="'.$imginfo['width'].'" ';
    if(isset($h)) $cssRule .= ' height="'.$imginfo['height'].'" ';
    $cssRule .= " />";
    //$cssRule.=" height:".$imginfo['height']."px; ";
    echo $cssRule."\n";
}


function _ibx($lb,$classes=''){
    $lb2 = str_ireplace('.','_',$lb);
    echo ' class="imgbox_'.$lb2.' imgbox '.$classes.'" ';
}
function _ibx_class($lb){
    $lb2 = str_ireplace('.','_',$lb);
    echo 'imgbox_'.$lb2.' imgbox ';
}

function _ibb($lb,$classes=''){
    $lb2 = str_ireplace('.','_',$lb);
    echo ' class="imgback_'.$lb2.' imgback '.$classes.'" ';
}
function _ibb_class($lb){
    $lb2 = str_ireplace('.','_',$lb);
    echo 'imgback_'.$lb2.' imgback ';
}

function _itg($lb, $alt='', $w=null, $h=null){
    __imgTag($lb,$alt,$w,$h);
}

function _ibk($lb,$setSize=false){
    __imgCssBackground($lb,$setSize);
}

function _iurl($lb){
    global $T;
    echo $T->imgBase64($lb);
}


function _inclCssReplaceParent($path, $parent, $replace){
    ob_start();
    include($path);
    $css_body = ob_get_contents();
    ob_end_clean();
    $css_body = str_ireplace($parent, $replace, $css_body);
    echo $css_body;
}




















