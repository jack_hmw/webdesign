<?php

class HTML_Elements {

    public $export = false;
    public $maxWidth = 900;

    public $CSS = array(
        'base_color1' => '#444444',
        'base_color2' => '#0055AA',
        'base_color3' => '#62B3E0',
        'alt_color1' => '#ED6A65',
        //'' => ''
    );

    function __construct(){
        global $TemplateManagerCfg;
        if(!isset($TemplateManagerCfg)) $TemplateManagerCfg=array();
        if(isset($TemplateManagerCfg['export'])) $this->export=$TemplateManagerCfg['export'];
    }

    function _export_process($c){
        if($this->export===true){
            $c =  str_ireplace(array('[',']'),array('{ldelim}','{rdelim}'),$c);
        }
        return $c;
    }

    function section($name, $extra=''){
        //echo '<table '.$extra.' cellspacing="0" cellpadding="0"><tr><td>'."\n";
        ob_start();
        include("./sections/" . $name . ".php");
        $c = ob_get_contents();
        ob_end_clean();
        $c = $this->_export_process($c);
        echo $c;
        //echo '</td></tr></table>'."\n";
    }

    function section_open($extra=''){
        echo '<table '.$extra.' cellspacing="0" cellpadding="0"><tr><td>'."\n";
    }

    function section_close(){
        echo '</td></tr></table>'."\n";
    }

    function element($name, $content=array(), $btn_css=array()){
        $globalCss = $this->CSS;
        ob_start();
        include("./elements/" . $name . ".php");
        $c = ob_get_contents();
        ob_end_clean();
        $c = $this->_export_process($c);
        echo $c;
    }

}


$HTML = new HTML_Elements();


function _responsiveCss(){
    global $HTML; ?>
    .rsplargecell { width:100%; }
    .rspmainct { width:<?= $HTML->maxWidth; ?>px; }

    .rspcell33{ width: <?= round(($HTML->maxWidth*0.33-1),0,PHP_ROUND_HALF_DOWN); ?>px; }
    .rspcell33p{ width: <?= round(($HTML->maxWidth*0.33-1)-33,0,PHP_ROUND_HALF_DOWN); ?>px; padding:8px 16px 8px 16px; }

    .rspcell40{ width: <?= round(($HTML->maxWidth*0.4-1),0,PHP_ROUND_HALF_DOWN); ?>px; }
    .rspcell40p{ width: <?= round(($HTML->maxWidth*0.4-1)-33,0,PHP_ROUND_HALF_DOWN); ?>px; padding:8px 8px 8px 24px; }

    .rspcell50{ width: <?= $HTML->maxWidth/2-1; ?>px; }
    .rspcell50p{ width: <?= $HTML->maxWidth/2-49; ?>px; padding:8px 24px 8px 24px; }

    .rspcell60{ width: <?= round(($HTML->maxWidth*0.6-1),0,PHP_ROUND_HALF_DOWN); ?>px; }
    .rspcell60p{ width: <?= round(($HTML->maxWidth*0.6-1)-33,0,PHP_ROUND_HALF_DOWN); ?>px; padding:8px 12px 8px 20px; }

    .rspcell66{ width: <?= round(($HTML->maxWidth*0.66-1),0,PHP_ROUND_HALF_DOWN); ?>px; }
    .rspcell66p{ width: <?= round(($HTML->maxWidth*0.66-1)-33,0,PHP_ROUND_HALF_DOWN); ?>px; padding:8px 12px 8px 20px; }

    @media (min-width: <?= $HTML->maxWidth+50; ?>px) {
        .mobile-only { display:none !important; }
    }

    @media (max-width: <?= $HTML->maxWidth+50-1; ?>px) {
        .rspmainct { width:<?= $HTML->maxWidth/2; ?>px !important; }
        .rspcell33_ct { width: <?= round(($HTML->maxWidth/2),0,PHP_ROUND_HALF_DOWN); ?>px !important; }
        .rspcell33{ display: block !important; width:<?= $HTML->maxWidth/2; ?>px !important; }
        .rspcell33p{ display: block !important; width:<?= $HTML->maxWidth/2-33; ?>px !important; }
        .rspcell40{ display: block !important; width:<?= $HTML->maxWidth/2; ?>px !important; }
        .rspcell40p{ display: block !important; width:<?= $HTML->maxWidth/2-33; ?>px !important; }
        .rspcell50{ display: block !important; }
        .rspcell50p{ display: block !important; }
        .rspcell60{ display: block !important; width:<?= $HTML->maxWidth/2; ?>px !important; }
        .rspcell60p{ display: block !important; width:<?= $HTML->maxWidth/2-33; ?>px !important; }
        .rspcell66{ display: block !important; width:<?= $HTML->maxWidth/2; ?>px !important; }
        .rspcell66p{ display: block !important; width:<?= $HTML->maxWidth/2-33; ?>px !important; }
        .desktop-only { display:none !important; }
    }
<?php
}


function _includeMobileCss($mobcss=null){
    global $HTML; ?>

    @media (max-width: <?= $HTML->maxWidth+50-1; ?>px) {
    <?php if($mobcss!=null) include($mobcss); echo "\n"; ?>
    }
    <?php
}


function _rMC($classes=''){
    global $HTML;
    echo ' class="rspmainct '.$classes.'" width="' .($HTML->maxWidth). '" ';
}

function _rLC($classes=''){
    //global $HTML;
    echo ' class="rsplargecell '.$classes.'" ';
}

function _rSC40($classes=''){
    global $HTML;
    echo ' class="rspcell40 '.$classes.'" width="' .(round(($HTML->maxWidth*0.4-1),0,PHP_ROUND_HALF_DOWN)). '" ';
}

function _rSC40p($classes=''){
    global $HTML;
    echo ' class="rspcell40p '.$classes.'" width="' .(round(($HTML->maxWidth*0.4-1)-33,0,PHP_ROUND_HALF_DOWN)). '" ';
}

function _rSC50($classes=''){
    global $HTML;
    echo ' class="rspcell50 '.$classes.'" width="' .($HTML->maxWidth/2-1). '" ';
}

function _rSC60($classes=''){
    global $HTML;
    echo ' class="rspcell60 '.$classes.'" width="' .(round(($HTML->maxWidth*0.6-1),0,PHP_ROUND_HALF_DOWN)). '" ';
}

function _rSC60p($classes=''){
    global $HTML;
    echo ' class="rspcell60p '.$classes.'" width="' .(round(($HTML->maxWidth*0.6-1)-33,0,PHP_ROUND_HALF_DOWN)). '" ';
}

function _rSC66($classes=''){
    global $HTML;
    echo ' class="rspcell66 '.$classes.'" width="' .(round(($HTML->maxWidth*0.66-1),0,PHP_ROUND_HALF_DOWN)). '" ';
}

function _rSC66p($classes=''){
    global $HTML;
    echo ' class="rspcell66p '.$classes.'" width="' .(round(($HTML->maxWidth*0.66-1)-33,0,PHP_ROUND_HALF_DOWN)). '" ';
}

function _rSC33($classes=''){
    global $HTML;
    echo ' class="rspcell33 '.$classes.'" width="' .round(($HTML->maxWidth/3),0,PHP_ROUND_HALF_DOWN). '" ';
}

function _rSC33p($classes=''){
    global $HTML;
    echo ' class="rspcell33p '.$classes.'" width="' .round(($HTML->maxWidth/3)-33,0,PHP_ROUND_HALF_DOWN). '" ';
}

function _rSC50p($classes=''){
    global $HTML;
    echo ' class="rspcell50p '.$classes.'" width="' .round(($HTML->maxWidth/2)-49,0,PHP_ROUND_HALF_DOWN). '" ';
}
