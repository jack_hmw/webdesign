<?php

// $_GET['export']
// $_GET['gnbr_freeform']

include('../inc/TemplateManager.class.php');
//https://www.hulpmiddelwereld.nl/media/wysiwyg/ico_flower.png
global $T;
if(isset($_GET['export'])){
    $T->imgBasePrefix = '{{media url="wysiwyg/';
    $T->imgBaseSuffix = '"}}';
}
?>

<?php if(!isset($_GET['export'])):?>
<script type="text/javascript" src="http://webdesign.test/inc/jquery.min.js"></script>
<script type="text/javascript" src="http://webdesign.test/inc/slickslider.jquery.js"></script>

<?php else: ?>

    <?php if(isset($_GET['gnbr_freeform'])): ?>
<script type="text/javascript" src="/js/gn/brochure-request.js"></script>
<script type="text/javascript" src="/js/gn/postcode-nl-api.js"></script>
    <?php endif; ?>

<!-- jquery plugin for slider -->
<script type="text/javascript" src="/js/slick/slick.min.js"></script>

<?php endif; ?>

<style type="text/css">
    <?php _inclCssReplaceParent('./css/common.css','.bb_lngpg', '.bb_lngpg'); ?>

    <?php _inclCssReplaceParent('./css/style.css', '.bb_lngpg', '.bb_lngpg'); ?>

    <?php //include('./mobile.css'); ?>

    <?php __imgBoxCss('quote_top1.png',null,'12%'); ?>
    <?php __imgBoxCss('quote_bottom1.png',null,'12%'); ?>

    <?php $bbico_size='100px'; ?>
    <?php __imgBackCss('BBrain_logo_mobile.png'); ?>

    <?php __imgBoxCss('bbico_Afgenomen_tijdsbesef.png',null,$bbico_size,$bbico_size); ?>
    <?php __imgBoxCss('bbico_Contact_verbinding.png',null,$bbico_size,$bbico_size); ?>
    <?php __imgBoxCss('bbico_Eenzaamheid.png',null,$bbico_size,$bbico_size); ?>
    <?php __imgBoxCss('bbico_Noodzaak_plezier.png',null,$bbico_size,$bbico_size); ?>
    <?php __imgBoxCss('bbico_Structuur_planning.png',null,$bbico_size,$bbico_size); ?>
    <?php __imgBoxCss('bbico_Zelfredzaamheid.png',null,$bbico_size,$bbico_size); ?>

    <?php __imgBoxCssResize('ba_bbrain_Tijdstip1.png',null,null, 280); ?>
    <?php __imgBoxCssResize('ba_bbrain_Agenda.png',null,null, 280); ?>
    <?php __imgBoxCssResize('ba_bbrain_Berichten.png',null,null, 280); ?>
    <?php __imgBoxCssResize('ba_bbrain_fotos-new.png',null,null, 280); ?>

    <?php __imgBoxCssResize('Janine_bblp092018.png',null,null, 112); ?>
    <?php __imgBoxCssResize('Robert_bblp092018.png',null,null, 112); ?>
    <?php __imgBoxCssResize('Yvonne_bblp092018.png',null,null, 112); ?>

    <?php //__imgBoxCss('logo_hmw.png'); ?>
    <?php //__imgBackCss('uitph1.jpg',null,array('setheight'=>true)); ?>
    <?php __imgBackCss('bigflatarrows2.png'); ?>
    <?php __imgBackCss('klok_room.jpg'); ?>
    @media screen and (min-width: 768px) {
        <?php __imgBackCss('ba_Banner-BBrain3.jpg'); ?>
        .bb_lngpg .mainphoto { background-size:contain; }
    }
    #watmaaktuniek .sect_ico .ico {
    <?php __imgCssBackground('bblp1_icons.png'); ?>
    }

</style>

<div class="bb_lngpg">

    <?php include('sections/direct_bestellen.php'); ?>

    <?php include('sections/reviews_stripe.php'); ?>

    <?php include('sections/instrvideo.php'); ?>

    <?php include('sections/welke_functies.php'); ?>

    <?php include('sections/watmaaktuniek.php'); ?>

    <?php include('sections/reviews.php'); ?>

    <?php include('sections/ftbanner.php'); ?>

    <?php include('temp_fixes/reviews_no_faces.php'); ?>

</div>



<script type="text/javascript">// <![CDATA[
    // doc: http://www.landmarkmlp.com/js-plugin/owl.carousel/#customizing

    jQuery(function ($) {
        if(HMW.windowWidthCompare(499)<0){
            HMW.textBoxToggler_T1({
                selector:'#direct_bestellen .rt .ct',
                min:210,
                speed:300
            });
        }

        var sliderDiv$ = jQuery("#bekijkvideo section.mainslider");
        sliderDiv$.css({
            'max-height': '600px'
        });
        sliderDiv$.slick(
            {
                dots: false,
                arrows: true,
                centerMode: true,
                centerPadding: '0px', //change to show slides on the sides
                variableWidth: false,
                slidesToShow: 1
            }
        );
        setTimeout(function(){
            jQuery("#bekijkvideo section.mainslider.slick-slider .slick-prev").addClass('<?php _ibb_class('bigflatarrows2.png'); ?>');
            jQuery("#bekijkvideo section.mainslider.slick-slider .slick-next").addClass('<?php _ibb_class('bigflatarrows2.png'); ?>');
        },2000);

        if(HMW.windowWidthCompare(767)<0){
            var reviewSliderDiv$ = jQuery("#klantervarigen .reviewsct");

            var maxH_Reviews = 0;
            jQuery('.bb_lngpg #klantervarigen .w33').each(function(){
                var thish = jQuery(this).height();
                console.log('x',thish);
                if(thish>maxH_Reviews) maxH_Reviews=thish;
            });
            console.log(maxH_Reviews);
            maxH_Reviews*=2;

            reviewSliderDiv$.css({
                'max-height': maxH_Reviews+'px'
            });
            reviewSliderDiv$.slick(
                {
                    dots: false,
                    arrows: true,
                    centerMode: true,
                    centerPadding: '0px', //change to show slides on the sides
                    variableWidth: false,
                    slidesToShow: 1
                }
            );
            setTimeout(function(){
                jQuery("#klantervarigen .reviewsct.slick-slider .slick-prev").addClass('<?php _ibb_class('bigflatarrows2.png'); ?>');
                jQuery("#klantervarigen .reviewsct.slick-slider .slick-next").addClass('<?php _ibb_class('bigflatarrows2.png'); ?>');
            },2000);
        }





        $('.topInternalLinks a').bind('click',function(e){
            e.preventDefault();
            $('html,body').animate({
                scrollTop: $($(this).attr('href')).offset().top-50 /*header=50px*/
            }, 'slow');
        });


    });
    // ]]></script>


<?php if(isset($_GET['gnbr_freeform'])): ?>
<style type="text/css">
    .gnbr-layout-B {
        padding: 2em;
    }
    .bb_lngpg .mainphoto {
        max-height: none;
        min-height: auto;
        height: auto;
        background-position-y:top;
    }
</style>
<?php endif; ?>