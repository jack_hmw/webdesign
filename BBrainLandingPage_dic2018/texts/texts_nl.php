<?php

return array(

    'quote1_t1' => 'Dankzij de BBrain Slimme Kalenderklok',
    'quote1_t2' => 'vergeet ik nooit meer om mijn medicatie in te nemen',

    'usps_t1_title1' => 'De BBrain Family G2 is uniek doordat deze klok:',
    'usps_t1_ico1' => 'Het gevoel van<br/>eenzaamheid<br/>verkleint',
    'usps_t1_ico2' => 'Overzicht biedt<br />bij afgenomen<br />tijdsbesef',
    'usps_t1_ico3' => 'Dagelijkse structuur<br/>&amp; planning biedt',
    'usps_t1_ico4' => 'Noodzaak &amp; plezier<br/>combineert',
    'usps_t1_ico5' => 'Zorgt voor contact<br/>&amp; verbinding',
    'usps_t1_ico6' => 'De zelfredzaamheid<br/>vergroot',

    'tintlink_1' => 'Waarom BBrain?',
    'tintlink_2' => 'Bekijk de video',
    'tintlink_3' => 'Alle functionaliteiten',
    'tintlink_4' => 'Klantervaringen',
    'tintlink_5' => 'Direct bestellen',

    'directbst1_title' => 'Helemaal bij tijd met de BBrain Slimme Kalenderklok',
    'directbst1_subtitle2' => 'eenvoudig en functioneel',
    'directbst1_text' => 'Naarmate mensen ouder worden, <strong>vergeten</strong> ze steeds vaker kleine dingen. Een afspraak met de dokter, het innemen van medicijnen of de (klein)kinderen die op bezoek komen. Daarnaast worden we minder mobiel en kunnen we niet meer overal bij zijn. Met de BBrain Family G2 sturen <strong>familieleden</strong>, vrienden en verzorgers eenvoudig: herinneringen & agendapunten, foto’s en leuke berichten naar de <strong>klok</strong>.',
    'directbst2_text' => 'Iedereen in de familie kan zo overal bij <strong>betrokken</strong> worden. Vakantiefoto’s bijvoorbeeld, deze stuurt u met 2 klikken naar de klok toe. Ditzelfde geldt voor een belangrijke afspraak (dokter, tandarts, medicijninname) of simpelweg een leuk bericht. De BBrain Family G2 wordt altijd onderdeel van de familie, alle <strong>generaties</strong> blijven zo ook op afstand betrokken. Niet te vergeten: zij zijn dan helemaal bij de tijd.',
    'directbst1_button' => 'Direct bestellen',

    'welkefnct_title' => 'De unieke functies van de BBrain Family G2',
    'welkefnct_subtitle2' => 'Simpel gehouden, voor maximaal gebruiksgemak',
    'welkefunct1_button' => 'Meer informatie of bestellen',

    'welkefnct_txt_10' => 'Datum, tijd en dagdeel',
    'welkefnct_txt_19' => 'Helpt bij afgenomen tijdsbesef',
    'welkefnct_txt_11' => 'Zorg voor een beter tijdsbesef met de BBrain Slimme Kalenderklok! Deze klok laat duidelijk de tijd, datum, dag en dagdeel zien. Met een blik op de klok weet de gebruiker hoe laat het is, welke dag het is en of het ochtend, middag of avond is. Hierdoor krijgt de gebruiker rust en zelfvertrouwen.',

    'welkefnct_txt_20' => 'Agendafunctie',
    'welkefnct_txt_29' => 'Biedt overzicht & structuur',
    'welkefnct_txt_21' => 'Losse briefjes of een papieren kalender worden overbodig met de BBrain Slimme Kalenderklok. Familie, vrienden en verzorgers kunnen eenvoudig via een app op hun telefoon of computer een afspraak of herinnering naar de klok sturen. Zo wordt nooit meer een afspraak vergeten!',

    'welkefnct_txt_30' => 'Berichtenfunctie',
    'welkefnct_txt_39' => 'Belangrijke reminder of een leuk bericht',
    'welkefnct_txt_31' => 'Met de berichtenfunctie kunnen familie, vrienden en verzorgers een snelle reminder of een leuk bericht naar deze slimme klok sturen. Herinner de gebruiker aan het innemen van zijn medicijnen of stuur een herinnering van een afspraak. De gebruiker ontvangt het bericht en de melding wordt ook direct uitgesproken. Zo kan geen boodschap gemist worden. ',

    'welkefnct_txt_40' => 'Fotofunctie',
    'welkefnct_txt_49' => 'Deel alle mooie momenten',
    'welkefnct_txt_41' => 'Dankzij de fotofunctie stuurt u eenvoudig foto’s naar de klok. Stuur een foto naar de klok terwijl u op vakantie bent of roep herinneringen op met een foto uit de oude doos. De beelden kunnen een vertrouwde werking hebben en het is voor de gebruiker leuk om zich betrokken te voelen bij uw dagelijkse activiteiten. Met een klik op de foto, activeert de gebruiker de fotolijstfunctie. ',

    'instrvideo_godw1' => 'Bekijk hieronder de instructievideo',
    'instrvideo_title' => 'Bekijk hoe eenvoudig het werkt',
    'instrvideo_t01' => 'Kijk de video en zie wat de mogelijkheden zijn!',
    'instrvideo_t02' => 'Klik op de pijl om de mogelijkheden per functie uitgelegd te krijgen.',
    'instrvideo_t03' => 'Klik op de pijltjes om de uitleg video\'s van de BBrain te bekijken',
    'instrvideo_t04' => 'Onderstaande video geeft een goed beeld van de BBrain',


    'reviews_title' => 'Wat zeggen anderen over de BBrain G2?',
    'reviews_subtitle' => 'Lees wat gebruikers vinden',

    'reviews_r11' => 'A. Biesmans',
    'reviews_r12' => 'Heeft de klok aangeschaft voor haar moeder',
    'reviews_r13' => 'Gekocht voor m’n moeder die er heel veel plezier mee heeft. Vooral de fotomomenten die ze nu mee krijgt en vroeger niet vind ze heel leuk. De agendafunctie gebruiken we ook voor verjaardagen, medicijnen en afspraken met de thuiszorg. Dat je op de app kunt zien welke afspraken er in de kalender staan is voor ons van grote meerwaarde.',

    'reviews_r21' => 'Dedal',
    'reviews_r22' => 'Heeft de klok aangeschaft voor haar moeder',
    'reviews_r23' => 'Klok met veel mogelijkheden om wat structuur te brengen in het dagelijkse leven van iemand met dementie. Alle informatie wordt ook uitgesproken (prettig voor slechtzienden) een analoge en digitale klok, sommige functies zijn naar wens uit te zetten. Uiteraard nog wat kleine verbeterpuntjes hier en daar.',

    'reviews_r31' => 'Marc',
    'reviews_r32' => 'Heeft de klok aangeschaft voor zijn vader',
    'reviews_r33' => 'De klok hadden wij netjes op tijd in huis. Ik heb het allemaal voor mijn vader geïnstalleerd en alles wees zich eigenlijk van zelf. Ook het gebruik van de applicatie en het menu in de klok zelf zijn zeer eenvoudig. Mijn vader heeft sinds wij de klok hebben aangeschaft veel meer houvast. Hij ziet het zelf niet als een hulpmiddel, maar zelfs meer als een toevoeging aan zijn woonkamer.',


    'ftbanner_title' => 'Betrek alle <strong>generaties</strong> op een unieke manier',
    'ftbanner_li1' => 'duidelijke weergave van datum, dagdeel &amp; tijdstip; met spraakfunctie',
    'ftbanner_li2' => 'stuur moeiteloos berichten, foto\'s &amp; agendapunten naar de klok',
    'ftbanner_li3' => 'draag als dierbare bij aan meer structuur, ori&euml;ntatie &amp; betrokkenheid',
    'ftbanner_btn' => '<span>Ga direct naar</span> <span>de productpagina <i class="fa fa-arrow-right" style="margin-left: 8px;"></i></span>',

    'watmaakt_txt_a1' => 'Wat maakt de BBrain uniek?',
    'watmaakt_txt_a2' => 'De BBrain Family G2 heeft, naast de algemene functies, een aantal mogelijkheden die het gebruik van de klok zo <strong>eenvoudig</strong> maken. Tijdens de ontwikkeling van dit unieke product is gedacht vanuit de toekomstige gebruiker van de klok. Wat is belangrijk voor deze mensen, welke <strong>behoeftes</strong> zijn er? Wat zijn functies die het leven echt een beetje gemakkelijker maken?',
    'watmaakt_txt_a3' => 'Door met deze gedachten in het achterhoofd te werken, zijn verschillende functies mogelijk gemaakt. Denk hierbij aan een gesloten systeem, een <strong>duidelijke</strong> spraakfunctie, een functie voor slechtzienden en de mogelijkheid tot het aan- en uitzetten van functies. Tevens kunt u verschillende thema’s instellen op de klok. Kortom: <strong>personaliseer</strong> naar behoefte en haal eruit wat erin zit met de BBrain.',
    'watmaakt_txt_10' => 'Een gesloten systeem',
    'watmaakt_txt_11' => 'De klok beschikt over een gesloten systeem. Hierdoor is het voor de gebruiker niet mogelijk om instellingen onverhoopt aan te passen. Dankzij de integratie van dit systeem, komt de gebruiker niet voor verassingen of onbekende situaties te staan.',
    'watmaakt_txt_20' => 'Een duidelijke spraakfunctie',
    'watmaakt_txt_21' => 'De klok beschikt over een uitspraakfunctie voor de tijd, berichten en agendapunten. Ieder bericht wordt uitgesproken op het moment dat het binnenkomt. Heeft de gebruiker een melding gemist? Door op de melding te klikken, wordt deze nogmaals uitgesproken. Met een klik op de klokweergave worden dag, datum en tijd uitgesproken.',
    'watmaakt_txt_30' => 'Functies aan- en uitzetten',
    'watmaakt_txt_31' => 'Zijn bepaalde functies overbodig of heeft de klokgebruiker moeite met het beheren van bepaalde functies? Geen probleem! De meeste functies kunnen afzonderlijk van elkaar uitgeschakeld worden.',
    'watmaakt_txt_40' => 'Contrast voor slechtzienden',
    'watmaakt_txt_41' => 'Veel gebruikers ondervinden problemen met het zicht. Om deze reden hebben we een grote klok ontworpen die geschikt is voor mensen met een visuele beperking. Er zijn thema’s beschikbaar op de klok die voor een groot contrast zorgen, zodat de klok goed leesbaar blijft.',
);



