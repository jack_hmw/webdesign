<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>BBrain Landing Page</title>
    <style type="text/css">
        body {
            background-color:#dddddd;
        }

        <?php include('./css/for_tests.css');  ?>

        .fakecontainer {
            width:100%;
            max-width: 1200px;
            margin: 0 auto;
            background-color: #ffffff;
        }
        @media screen and (max-width: 1199px) {
            .fakecontainer { max-width: 960px; }
        }
        @media screen and (max-width: 991px) {
            .fakecontainer { max-width: 740px; }
        }
        @media screen and (max-width: 767px) {
            .fakecontainer { max-width: unset; }
        }
    </style>
</head>

<body>
<div class="fakecontainer">
    <?php include('./body.php');  ?>
</div>
</body>
</html>
