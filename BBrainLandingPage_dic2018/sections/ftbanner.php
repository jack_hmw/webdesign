
<div class="blocks columns nopadding-lr" id="ftbanner">
    <div class="wp33">
        <div <?php _ibx('ba_bbrain_Berichten.png'); ?>></div>
    </div>
    <div class="wp66">
        <div>
            <h3 style="text-align: left;"><?php __e('ftbanner_title'); ?></h3>
            <ul>
                <li><i class="fa fa-check"></i> <span><?php __e('ftbanner_li1'); ?></span></li>
                <li><i class="fa fa-check"></i> <span><?php __e('ftbanner_li2'); ?></span></li>
                <li><i class="fa fa-check"></i> <span><?php __e('ftbanner_li3'); ?></span></li>
            </ul>
            <a href="<?php __lk('directbst1_button'); ?>" class="button_lp button_t3" style="margin-top:1.6em;padding-left: 2em;">
                <?php __e('ftbanner_btn'); ?>
            </a>
        </div>
    </div>
</div>
