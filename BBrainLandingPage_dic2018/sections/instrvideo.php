
<!-- imgback_ba_Banner-BBrain3_jpg imgback   imgback_klok_room_jpg imgback   mainphoto -->

<div class="blocks nopadding-lr" id="bekijkvideo">

    <div class="godw" style="display: none;">
        <h4><?php __e('instrvideo_godw1'); ?></h4>
        <a style="font-size: 28px;"><em class="fa fa-sort-down"></em></a>
    </div>

    <div class="blocks sqvideo nopadding">
        <div class="blocks sqvideo nopadding-tb">
            <h2><?php __e('instrvideo_title'); ?></h2>
            <div class="separator_h5" style="width:40%;margin-left:-16px;"></div>
            <h5 style="font-style: italic; text-align: center; font-weight: normal; margin-bottom:2em;"><?php __e('instrvideo_t04'); ?></h5>
        </div>

        <section class="mainslider onebig">
            <div class="mslidehh bekijkvideo">
                <div class="mslidehh flex1 showbeforeclick <?php _ibb_class('klok_room.jpg'); ?>">
                    <div class="mslidehhoverlay">
<!--                        <h3>BBrain Family G2</h3>-->
<!--                        <h5>De klok die met u mee verandert</h5>-->
                        <div class="bottom" style="bottom: 35%;"><i class="fa fa-play-circle" style="display:block;margin-bottom:0.6em;"></i> Bekijk video</div>
                    </div>
                </div>
                <div class="mslidehh showafterclick" style="display: none;">
                    <iframe id="bbrainvideo1" width="600" height="350" src="https://www.youtube.com/embed/f42UqdeDAAc"
                            frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<!--                    <iframe id="bbrainvideo1" width="600" height="350"-->
<!--                            src="https://www.youtube.com/embed/1iT4a-9Eo8c" frameborder="0"-->
<!--                            allow="autoplay; encrypted-media" allowfullscreen></iframe>-->
                </div>
            </div>
<!--            <div class="mslidehh media_ct">-->
<!--                <div class="mslidehh showbeforeclick --><?php //_ibb_class('klok_room.jpg'); ?><!--">-->
<!--                    <h3>BBrain Family G2</h3>-->
<!--                    <h5>De klok die met u mee verandert</h5>-->
<!--                    <div><i class="fa fa-play-circle"></i> Bekijk video</div>-->
<!--                </div>-->
<!--                <div class="mslidehh showafterclick" style="display: none;">-->
<!--                    <iframe id="bbrainvideo1" width="600" height="350"-->
<!--                            src="https://www.youtube.com/embed/1iT4a-9Eo8c" frameborder="0"-->
<!--                            allow="autoplay; encrypted-media" allowfullscreen></iframe>-->
<!--                </div>-->
<!--            </div>-->
        </section>

<!--        <p>--><?php //__e('instrvideo_t01'); ?><!--</p>-->
<!--        <p>--><?php //__e('instrvideo_t02'); ?><!--</p>-->
    </div>
</div>

<script type="text/javascript">// <![CDATA[
    // doc: http://www.landmarkmlp.com/js-plugin/owl.carousel/#customizing

    jQuery(function ($) {
        $('.showbeforeclick').bind('click',function(e){
            $('.showafterclick').css({
                'opacity':0,
                'display':'block',
                'position':'absolute',
                'top':0,
                'width': '100%'
            }).animate({
                opacity: 1
            }, 2000);
            $('.showbeforeclick').css({
                'position':'absolute',
                'top':0,
                'width': '100%'
            }).animate({
                opacity: 0
            }, 2000);
        });
    });
    // ]]></script>