
<div class="blocks" id="allefunction">
    <h2><?php __e('welkefnct_title'); ?></h2>
    <div class="separator_h5" style="width:40%;margin-left:-16px;"></div>
    <h5 style="font-style: italic; text-align: center; font-weight: normal;"><?php __e('welkefnct_subtitle2'); ?></h5>

    <div class="bclck_row">
        <div class="blocks columns bclck">
            <div class="w33">
                <span <?php _ibx('ba_bbrain_Tijdstip1.png'); ?>></span>
            </div>
            <div class="w66">
                <div class="txt">
                    <h4><?php __e('welkefnct_txt_10'); ?></h4>
                    <p class="italic"><?php __e('welkefnct_txt_19'); ?></p>
                    <p><?php __e('welkefnct_txt_11'); ?></p>
                </div>
            </div>
        </div>

        <div class="blocks columns bclck">
            <div class="w33">
                <span <?php _ibx('ba_bbrain_Agenda.png'); ?>></span>
            </div>
            <div class="w66">
                <div class="txt">
                    <h4><?php __e('welkefnct_txt_20'); ?></h4>
                    <p class="italic"><?php __e('welkefnct_txt_29'); ?></p>
                    <p><?php __e('welkefnct_txt_21'); ?></p>
                </div>
            </div>
        </div>
    </div>

    <div class="bclck_row">
        <div class="blocks columns bclck">
            <div class="w33">
                <span <?php _ibx('ba_bbrain_Berichten.png'); ?>></span>
            </div>
            <div class="w66">
                <div class="txt">
                    <h4><?php __e('welkefnct_txt_30'); ?></h4>
                    <p class="italic"><?php __e('welkefnct_txt_39'); ?></p>
                    <p><?php __e('welkefnct_txt_31'); ?></p>
                </div>
            </div>
        </div>

        <div class="blocks columns bclck">
            <div class="w33">
                <span <?php _ibx('ba_bbrain_fotos-new.png'); ?>></span>
            </div>
            <div class="w66">
                <div class="txt">
                    <h4><?php __e('welkefnct_txt_40'); ?></h4>
                    <p class="italic"><?php __e('welkefnct_txt_49'); ?></p>
                    <p><?php __e('welkefnct_txt_41'); ?></p>
                </div>
            </div>
        </div>
    </div>

    <div class="blocks buttonmeerinfo">
        <a href="<?php __lk('welkefunct1_button'); ?>" class="button_lp button_t1"><?php __e('welkefunct1_button'); ?>  <i class="fa fa-arrow-right" style="margin-left: 8px;"></i></a>
    </div>

</div>