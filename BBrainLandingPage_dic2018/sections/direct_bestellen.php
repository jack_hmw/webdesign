<h1 style="margin-top: 1em;">
    <div class="bblogo <?php _ibb_class('BBrain_logo_mobile.png'); ?>"></div>
    BBrain Family G2
</h1>

<div class="topInternalLinks">
    <a href="<?php __lk('tintlink_1'); ?>"><?php __e('tintlink_1'); ?></a>
    <a href="<?php __lk('tintlink_2'); ?>"><?php __e('tintlink_2'); ?></a>
    <a href="<?php __lk('tintlink_3'); ?>"><?php __e('tintlink_3'); ?></a>
    <a href="<?php __lk('tintlink_4'); ?>"><?php __e('tintlink_4'); ?></a>
    <a href="<?php __lk('tintlink_5'); ?>"><?php __e('tintlink_5'); ?></a>
</div>

<!--<div class="bigstaticbanner">-->
<!--    <img src="" />-->
<!--</div>-->

<div class="<?php
_ibb_class('ba_Banner-BBrain3.jpg');
?>  <?php
_ibb_class('klok_room.jpg');
?>  mainphoto">
<!--    <p class="quote">
        <span <?php _ibx('quote_top1.png','qtop'); ?>></span>
        <span class="qtext"><?php __e('quote1_t1'); ?></span>
        <span class="qtext"><?php __e('quote1_t2'); ?></span>
        <span <?php _ibx('quote_bottom1.png','qbottom'); ?>></span>
  </p>-->
<?php if(isset($_GET['gnbr_freeform'])): ?>
    <div class="gnbr_freeform1">
        {{block name="brochure_request_form" type="gn_brochurerequest/frontend_brochure_request_form_container" template="gn/brochure_request/form/container_layout_B.phtml" request_type_id="10"}}
    </div>
<?php endif; ?>
</div>

<div class="blocks usps_t1" id="waaroombbrain">
    <h2 style="text-align:center;"><?php __e('usps_t1_title1'); ?></h2>
    <div class="usps">
        <div class="usp">
            <span <?php _ibx('bbico_Eenzaamheid.png'); ?>></span>
            <span class="t"><?php __e('usps_t1_ico1'); ?></span>
        </div>
        <div class="usp">
            <span <?php _ibx('bbico_Afgenomen_tijdsbesef.png'); ?>></span>
            <span class="t"><?php __e('usps_t1_ico2'); ?></span>
        </div>
        <div class="usp">
            <span <?php _ibx('bbico_Structuur_planning.png'); ?>></span>
            <span class="t"><?php __e('usps_t1_ico3'); ?></span>
        </div>
        <div class="usp sep"></div>
        <div class="usp">
            <span <?php _ibx('bbico_Noodzaak_plezier.png'); ?>></span>
            <span class="t"><?php __e('usps_t1_ico4'); ?></span>
        </div>
        <div class="usp">
            <span <?php _ibx('bbico_Contact_verbinding.png'); ?>></span>
            <span class="t"><?php __e('usps_t1_ico5'); ?></span>
        </div>
        <div class="usp">
            <span <?php _ibx('bbico_Zelfredzaamheid.png'); ?>></span>
            <span class="t"><?php __e('usps_t1_ico6'); ?></span>
        </div>
    </div>
</div>


<div class="blocks nopadding" id="direct_bestellen">
    <div class="blocks">
        <div class="lt">
            <h2  style="text-align:center;margin-bottom: 1em;"><?php __e('directbst1_title'); ?></h2>
            <h5 style="font-style: italic; text-align: center; font-weight: normal; margin-bottom:3em;"><?php __e('directbst1_subtitle2'); ?></h5>
        </div>
        <div class="rt">
            <div class="ct">
                <div class="text">
                    <p><?php __e('directbst1_text'); ?></p>
                    <p><?php __e('directbst2_text'); ?></p>
                </div>
                <a href="#" class="more">Lees meer</a>
                <a href="#" class="less">Verbergen</a>
            </div>
        </div>

        <div class="blocks buttondirbest">
            <a href="<?php __lk('directbst1_button'); ?>" class="button_lp button_t1"><?php __e('directbst1_button'); ?> <i class="fa fa-arrow-right" style="margin-left: 8px;"></i></a>
        </div>
    </div>

</div>