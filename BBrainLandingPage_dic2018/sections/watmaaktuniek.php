
<div class="blocks" id="watmaaktuniek">
    <div class="blocks columns bclck nopadding-lr">
        <div class="w33 padding-lr">
            <h2><?php __e('watmaakt_txt_a1'); ?></h2>
            <div class="txt">
                <p><?php __e('watmaakt_txt_a2'); ?></p>
                <p><?php __e('watmaakt_txt_a3'); ?></p>
            </div>
        </div>
        <div class="w66">
            <div class="w66_listct">
                <div class="sect_ico si1">
                    <div class="ico"></div>
                    <h3><?php __e('watmaakt_txt_10'); ?></h3>
                    <p><?php __e('watmaakt_txt_11'); ?></p>
                </div>

                <div class="sect_ico si2">
                    <div class="ico"></div>
                    <h3><?php __e('watmaakt_txt_20'); ?></h3>
                    <p><?php __e('watmaakt_txt_21'); ?></p>
                </div>

                <div class="sect_ico si3">
                    <div class="ico"></div>
                    <h3><?php __e('watmaakt_txt_30'); ?></h3>
                    <p><?php __e('watmaakt_txt_31'); ?></p>
                </div>

                <div class="sect_ico si4">
                    <div class="ico"></div>
                    <h3><?php __e('watmaakt_txt_40'); ?></h3>
                    <p><?php __e('watmaakt_txt_41'); ?></p>
                </div>
            </div>
        </div>
    </div>

</div>