<?php

return array(

    'quote1_t1' => 'Dankzij de BBrain Slimme Kalenderklok',
    'quote1_t2' => 'vergeet ik nooit meer om mijn medicatie in te nemen',

    'usps_t1_ico1' => 'Eenzaamheid',
    'usps_t1_ico2' => 'Behoefte aan rust',
    'usps_t1_ico3' => 'Desorientatie',

    'directbst1_title' => 'Kent of herkend u de behoefte bij een van bovenstaande termen?',
    'directbst1_text' => 'De BBrain Slimme Kalenderklok is een klok welke speciaal is gemaakt voor senioren en hun naasten. Naarmate we ouder worden, vergeten we steeds vaker kleine dingen zoals afspraken met de dokter of met (klein)kinderen. Met de deze nieuwe BBrain klok kunnen kinderen, kleinkinderen en verzorgers overal ter wereld berichten, foto\'s en agendapunten naar de klok sturen. Daarbij kan het voor zorginstanties interessant zijn aangezien de klok u in staat stelt om een bericht naar meerdere personen te sturen. Zodra senioren een reminder nodig hebben voor het eten of medicatie, kunt u nu eenvoudig van afstand een bericht naar meerdere ontvangers sturen',
    'directbst1_button' => 'Direct bestellen!',

    'welkefnct_title' => 'Welke functies biedt de klok',
    'welkefnct_txt_10' => 'Datum, tijd & dagdeel',
    'welkefnct_txt_11' => 'De BBrain Slimme Kalenderklok geeft de gebruiker een beter besef van tijd en hierdoor meer structuur. Een vaker voorkomende opmerking van iemand die dichtbij een beginnend dementerend persoon staat is: “Zodra hij wakker wordt, dan weet hij niet meer welke dag het is.” Daarom heeft Hulpmiddelwereld de optie ontwikkeld waarbij de datum, de tijd en het dagdeel tegelijkertijd zichtbaar zijn. De gebruiker moet direct op de klok kunnen zien welke dag het is, hoe laat het is en of het ochtend, middag of avond is. Hierdoor zal de gebruiker minder verward zijn en meer zelfvertrouwen hebben.',
    'welkefnct_txt_20' => 'Agendafunctie',
    'welkefnct_txt_21' => 'Vaak wordt er nog gebruikgemaakt van een papieren kalender of losse briefjes waarop alle afspraken en medicijnherinneringen worden genoteerd. De BBrain Slimme Kalenderklok maakt de papieren kalender en losse briefjes overbodig. Nu kunnen familie, vrienden, mantelzorgers en indien nodig de klokgebruiker zelf <del>kunnen</del> eenvoudig, via een app op hun mobiele telefoon of via de computer, een afspraak of herinnering sturen naar de BBrain. Een perfect geheugensteuntje.',
    'welkefnct_txt_30' => 'Berichtenfunctie',
    'welkefnct_txt_31' => 'De Fitform Zorgfauteuils zijn speciaal door ons ontwikkeld voor mensen die meer eisen stellen aan zitten.',
    'welkefnct_txt_32' => 'Met de berichtenfunctie kunnen familie, vrienden of verzorgers snel een reminder of gewoon een leuk berichtje sturen. Laat de gebruiker weten dat je eraan komt of laat hem herinneren aan het innemen van de medicijnen. De gebruiker ontvangt het bericht waarbij de melding ook direct wordt uitgesproken. Zo zal de gebruiker het bericht niet missen.',
    'welkefnct_txt_40' => 'Fotofunctie',
    'welkefnct_txt_41' => 'De Fitform Zorgfauteuils zijn speciaal door ons ontwikkeld voor mensen die meer eisen stellen aan zitten',
    'welkefnct_txt_42' => 'De fotofunctie is toegevoegd om naast de functionele mogelijkheden die de BBrain Slimme Kalenderklok biedt ook een emotioneel aspect te kunnen bieden dat vertrouwd voelt voor de gebruiker. Laat de gebruiker meegenieten van uw vakantie of stuur een foto van een oude herinnering. Door de fotofunctie wordt de gebruiker betrokken bij uw dagelijkse activiteiten. Door op de foto te klikken wordt de fotolijstfunctie geactiveerd. In onderstaande video ziet u hoe de verschillende functionaliteiten in de praktijk werken.',

    'instrvideo_godw1' => 'Bekijk hieronder de instructievideo',
    'instrvideo_title' => 'Bekijk hoe de functionaliteiten werken',
    'instrvideo_t01' => 'Kijk de video en zie wat de mogelijkheden zijn!',
    'instrvideo_t02' => 'Klik op de pijl om de mogelijkheden per functie uitgelegd te krijgen.',

    'reviews_title' => 'Wat vinden de kopers',
    'reviews_r11' => 'Janine',
    'reviews_r12' => 'Heeft de klok aangeschaft voor haar moeder',
    'reviews_r13' => 'Ik zag de BBrain langskomen en het product leek me direct geschikt voor mijn moeder.
De laatste weken is haar tijdsbesef achteruit aan het gaan en ze vergeet kleine dingen.
De agendafunctie en het duidelijk weergeven van de tijd helpt daarbij.
Een heel fijn product!!',
    'reviews_r21' => 'Robert',
    'reviews_r22' => 'Heeft de klok voor zijn vader gekocht',
    'reviews_r23' => 'Mijn vader is beginnend dementerend. Na het slapen is hij altijd erg verward.
Het dementiethema van de klok is voor hem heel prettig en een goede houvast.
De klok maakt hem een stuk zelfverzekerder.',
    'reviews_r31' => 'Yvonne',
    'reviews_r32' => 'Heeft de klok aangeschaft voor haar gezin',
    'reviews_r33' => 'In een gezinssamenstelling van 6 personen, is het nogal lastig om de structuur en overzicht te behouden.
Wij gebruiken de klok als een digitale agenda voor alle afspraken. Heel handig!',

    'ftbanner_title' => 'De klok die uw leven <strong>veranderd</strong>',
    'ftbanner_li1' => 'Spreekt de datum & tijd uit',
    'ftbanner_li2' => 'Stuurt herinneringen voor belangrijke afspraken',
    'ftbanner_li3' => 'Blijf in contact met uw kinderen met de berichten en foto functie',
    'ftbanner_btn' => 'Bestel de BBrain klok direct',
);


