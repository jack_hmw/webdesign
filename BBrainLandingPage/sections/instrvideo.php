
<div class="blocks nopadding" id="instrvideo">

    <div class="godw">
        <h4><?php __e('instrvideo_godw1'); ?></h4>
        <a style="font-size: 28px;"><em class="fa fa-sort-down"></em></a>
    </div>

    <div class="blocks sqvideo">
        <h2><?php __e('instrvideo_title'); ?></h2>
        <div class="separator_h5" style="width:40%;margin-left:-16px;"></div>

        <div class="media_ct" style="">
            <iframe id="bbrainvideo1" width="600" height="350"
                    src="https://www.youtube.com/embed/1iT4a-9Eo8c" frameborder="0"
                    allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>

        <p><?php __e('instrvideo_t01'); ?></p>
        <p><?php __e('instrvideo_t02'); ?></p>
    </div>
</div>