
<div class="blocks" id="welke_functies">
    <h2><?php __e('welkefnct_title'); ?></h2>
    <div class="separator_h5" style="width:40%;margin-left:-16px;"></div>

    <div class="blocks columns bclck">
        <div class="w33">
            <span <?php _ibx('bclock1.jpg'); ?>></span>
        </div>
        <div class="w66">
            <div class="txt">
                <h4><?php __e('welkefnct_txt_10'); ?></h4>
                <?php __e('welkefnct_txt_11'); ?>
            </div>
        </div>
    </div>

    <div class="blocks columns bclck">
        <div class="w33">
            <span <?php _ibx('bclock2.jpg'); ?>></span>
        </div>
        <div class="w66">
            <div class="txt">
                <h4><?php __e('welkefnct_txt_20'); ?></h4>
                <?php __e('welkefnct_txt_21'); ?>
            </div>
        </div>
    </div>

    <div class="blocks columns bclck">
        <div class="w33">
            <span <?php _ibx('bclock3.jpg'); ?>></span>
        </div>
        <div class="w66">
            <div class="txt">
                <h4><?php __e('welkefnct_txt_30'); ?></h4>
                <p><?php __e('welkefnct_txt_31'); ?></p>
                <p><?php __e('welkefnct_txt_32'); ?></p>
            </div>
        </div>
    </div>

    <div class="blocks columns bclck">
        <div class="w33">
            <span <?php _ibx('bclock4.jpg'); ?>></span>
        </div>
        <div class="w66">
            <div class="txt">
                <h4><?php __e('welkefnct_txt_40'); ?></h4>
                <p><?php __e('welkefnct_txt_41'); ?></p>
                <p><?php __e('welkefnct_txt_42'); ?></p>
            </div>
        </div>
    </div>
</div>