<div <?php _ibb('klok_room.jpg','mainphoto'); ?>>
    <p class="quote">
        <span <?php _ibx('quote_top1.png','qtop'); ?>></span>
        <span class="qtext"><?php __e('quote1_t1'); ?></span>
        <span class="qtext"><?php __e('quote1_t2'); ?></span>
        <span <?php _ibx('quote_bottom1.png','qbottom'); ?>></span>
    </p>
</div>

<div class="blocks usps_t1">
    <div class="usps">
        <div class="usp">
            <span <?php _ibx('ico_user.png'); ?>></span>
            <span class="t"><?php __e('usps_t1_ico1'); ?></span>
        </div>
        <div class="usp">
            <span <?php _ibx('ico_flower.png'); ?>></span>
            <span class="t"><?php __e('usps_t1_ico2'); ?></span>
        </div>
        <div class="usp">
            <span <?php _ibx('ico_userq.png'); ?>></span>
            <span class="t"><?php __e('usps_t1_ico3'); ?></span>
        </div>
    </div>
</div>


<div class="blocks nopadding-lr" id="direct_bestellen">
    <div class="blocks columns">
        <div class="w25 lt"><h3><?php __e('directbst1_title'); ?></h3></div>
        <div class="w75 rt">
            <div class="ct"><?php __e('directbst1_text'); ?></div>
        </div>
    </div>
    <div class="blocks" style="text-align: center;">
        <a href="<?php __lk('directbst1_button'); ?>" class="button fill button_t1"><?php __e('directbst1_button'); ?></a>
    </div>
</div>