<?php
include('../inc/TemplateManager.class.php');
//https://www.hulpmiddelwereld.nl/media/wysiwyg/ico_flower.png
global $T;
if(isset($_GET['export'])){
    $T->imgBasePrefix = '{{media url="wysiwyg/';
    $T->imgBaseSuffix = '"}}';
}
?>
<style type="text/css">
    <?php _inclCssReplaceParent('./css/common.css','.bb_lngpg', '.bb_lngpg'); ?>

    <?php _inclCssReplaceParent('./css/style.css', '.bb_lngpg', '.bb_lngpg'); ?>

    <?php //include('./mobile.css'); ?>

    <?php __imgBackCss('klok_room.jpg'); ?>
    <?php __imgBoxCss('quote_top1.png',null,'12%'); ?>
    <?php __imgBoxCss('quote_bottom1.png',null,'12%'); ?>

    <?php __imgBoxCss('ico_flower.png'); ?>
    <?php __imgBoxCss('ico_user.png'); ?>
    <?php __imgBoxCss('ico_userq.png'); ?>

    <?php __imgBoxCssResize('bclock1.jpg',null,null, 180); ?>
    <?php __imgBoxCssResize('bclock2.jpg',null,null, 180); ?>
    <?php __imgBoxCssResize('bclock3.jpg',null,null, 180); ?>
    <?php __imgBoxCssResize('bclock4.jpg',null,null, 180); ?>

    <?php __imgBoxCssResize('Janine_bblp092018.png',null,null, 110); ?>
    <?php __imgBoxCssResize('Robert_bblp092018.png',null,null, 110); ?>
    <?php __imgBoxCssResize('Yvonne_bblp092018.png',null,null, 110); ?>

    <?php __imgBoxCssResize('bclockft.jpg',null,null, 160); ?>

    <?php //__imgBoxCss('logo_hmw.png'); ?>
    <?php //__imgBackCss('uitph1.jpg',null,array('setheight'=>true)); ?>
</style>

<div class="bb_lngpg">

    <?php include('sections/direct_bestellen.php'); ?>

    <?php include('sections/welke_functies.php'); ?>

    <?php include('sections/instrvideo.php'); ?>

    <?php include('sections/reviews.php'); ?>

    <?php include('sections/ftbanner.php'); ?>

</div>