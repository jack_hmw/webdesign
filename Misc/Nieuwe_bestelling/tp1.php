<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Nieuwe Bestelling TP1</title>
<style type="text/css">
<?php include('./tp1.css'); ?>
</style>
</head>
<?php

$placeholders = [
    '{{store url=""}}'=>'#',
    '{{skin url="images/logo_white_plus_mail.png" _area=\'frontend\'}}'=>'./logo-mail.png',
    '{{var store.getFrontendName()}}'=>'Hulpmiddelwereld',
    '{{htmlescape var=$order.getCustomerName()}}'=>'Meneer H.G. Meijers',
    '{{store url="customer/account/"}}'=>'#',
    '{{config path=\'trans_email/ident_support/email\'}}'=>'info@hulpmiddelwereld.nl',
    '{{var order.increment_id}}'=>'100020859',
    '{{var order.getCreatedAtFormated(\'long\')}}'=>'6 september 2016 14:26:58 CEST',
    '{{var order.getBillingAddress().format(\'html\')}}'=>'Meneer F. Talmon<br/>
Oostrandpark 46<br/>
8212AR Lelystad, Nederland<br/>
T: 0320 221965',
    '{{var payment_html}}'=>'<img src="./blue-black.png" />',
    '{{depend order.getIsNotVirtual()}}'=>'',
    '{{var order.getShippingAddress().format(\'html\')}}'=>'Meneer F. Talmon<br/>
Oostrandpark 46<br/>
8212AR Lelystad, Nederland<br/>
T: 0320 221965',
    '{{var order.getShippingDescription()}}'=>'Verzending',
    '{{/depend}}'=>'',
    '{{var order.getEmailCustomerNote()}}'=>'Email customer note
    new line text text text',
    '{{layout handle="sales_email_order_items" order=$order}}'=>'<table width="650" border="0" cellspacing="0" cellpadding="0" style="width:650px;border:1px solid #EAEAEA;">
<tbody><tr>
<td align="left" style="text-align:justify;background-color:#EAEAEA;padding:3px 9px;">
<font size="1"><span style="font-size:13px;background-color:#EAEAEA;">Artikel</span></font></td>
<td align="left" style="text-align:justify;background-color:#EAEAEA;padding:3px 9px;">
<font size="1"><span style="font-size:13px;background-color:#EAEAEA;">Artikelnummer</span></font></td>
<td align="center" style="text-align:center;background-color:#EAEAEA;padding:3px 9px;">
<font size="1"><span style="font-size:13px;background-color:#EAEAEA;">Aantal</span></font></td>
<td align="right" style="background-color:#EAEAEA;padding:3px 9px;"><font size="1"><span style="font-size:13px;background-color:#EAEAEA;">Subtotaal</span></font></td>
</tr>
<tr>
<td align="left" valign="top" style="text-align:justify;padding:3px 9px;border-bottom:1px dotted #CCCCCC;">
<font size="1"><span style="font-size:11px;"><font size="1"><span style="font-size:11px;"><b>Puck KeySafe Sleutelkluis</b></span></font> </span></font></td>
<td align="left" valign="top" style="text-align:justify;padding:3px 9px;border-bottom:1px dotted #CCCCCC;">
<font size="1"><span style="font-size:11px;">37059</span></font></td>
<td align="center" valign="top" style="text-align:center;padding:3px 9px;border-bottom:1px dotted #CCCCCC;">
<font size="1"><span style="font-size:11px;">1</span></font></td>
<td align="right" valign="top" style="padding:3px 9px;border-bottom:1px dotted #CCCCCC;">
<font size="1"><span style="font-size:11px;">€&nbsp;179,10 </span></font></td>
</tr>
<tr>
<td colspan="3" align="right" style="padding:3px 9px;">Subtotaal </td>
<td align="right" style="padding:3px 9px;">€&nbsp;179,10 </td>
</tr>
<tr>
<td colspan="3" align="right" style="padding:3px 9px;">Verzending en verwerking </td>
<td align="right" style="padding:3px 9px;">€&nbsp;0,00 </td>
</tr>
<tr>
<td colspan="3" align="right" style="padding:3px 9px;"><b>Totaal (excl. BTW)</b> </td>
<td align="right" style="padding:3px 9px;"><b>€&nbsp;148,02</b> </td>
</tr>
<tr>
<td colspan="3" align="right" style="padding:3px 9px;">NL standard VAT (21%) <br></td>
<td align="right" style="padding:3px 9px;">€&nbsp;31,08 </td>
</tr>
<tr>
<td colspan="3" align="right" style="padding:3px 9px;">BTW </td>
<td align="right" style="padding:3px 9px;">€&nbsp;31,08</td>
</tr>
<tr>
<td colspan="3" align="right" style="padding:3px 9px;"><b>Totaal (incl. BTW)</b> </td>
<td align="right" style="padding:3px 9px;"><b>€&nbsp;179,10</b> </td>
</tr>
</tbody></table>',
    ''=>'',
    ''=>'',
    ''=>'',
];

$file = file_get_contents('./tp1.html', true);

foreach ($placeholders as $k=>$v) {
    $file = str_ireplace($k,$v,$file);
}
echo $file;

?>
</html>