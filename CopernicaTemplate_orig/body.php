<?php

global $TemplateManagerCfg;
if(!isset($TemplateManagerCfg)) $TemplateManagerCfg=array();
$TemplateManagerCfg['lang']='nl';
//$TemplateManagerCfg['imgToBase64']=true;

include('../inc/TemplateManager.class.php');

?>
<style type="text/css">
    <?php include('./css/common.css'); ?>

    <?php include('./css/style.css'); ?>

    <?php include('./css/mobile.css'); ?>

    <?php __imgBoxCss('logo_hmw.png'); ?>
    <?php __imgBoxCss('ico_truck.png'); ?>
    <?php __imgBoxCss('ico_garantie.png'); ?>
    <?php __imgBoxCss('ico_calendar.png'); ?>
    <?php __imgBoxCss('ico_box.png'); ?>
    <?php __imgBackCss('klok_room.png'); ?>

    <?php __imgBackCss('topcat1.jpg'); ?>
    <?php __imgBackCss('topcat2.jpg'); ?>
    <?php __imgBackCss('topcat3.jpg'); ?>

    <?php __imgBoxCss('ico_lamp_red.png'); ?>
    <?php __imgBoxCss('ico_rewind_red.png'); ?>
    <?php __imgBoxCss('ico_garantie_red.png'); ?>

    <?php __imgBackCss('uitph1.jpg',null,array('setheight'=>true)); ?>
    <?php __imgBackCss('uitph2.jpg',null,array('setheight'=>true)); ?>
    <?php __imgBackCss('video_img1.jpg',null,array('setheight'=>true)); ?>
    <?php __imgBackCss('prod_img8.jpg',null,array('setheight'=>false)); ?>

    <?php __imgBoxCss('prod_img1.jpg'); ?>
    <?php __imgBoxCss('prod_img2.jpg'); ?>
    <?php __imgBoxCss('prod_img3.jpg'); ?>
    <?php __imgBoxCss('prod_img4.jpg'); ?>
    <?php __imgBoxCss('prod_img5.jpg'); ?>
    <?php __imgBoxCss('prod_img6.jpg'); ?>
    <?php __imgBoxCss('prod_img7.jpg'); ?>

    <?php __imgBoxCss('ico_plus_green.png'); ?>
    <?php __imgBoxCss('ico_star1_full.png'); ?>
    <?php __imgBoxCss('ico_star1_half.png'); ?>
    <?php __imgBoxCss('ico_star1_empty.png'); ?>

    <?php __imgBoxCss('footer_guy.png'); ?>
    <?php __imgBoxCss('color_review.png'); ?>
    <?php __imgBoxCss('ico_tel.png'); ?>
    <?php __imgBoxCss('ico_mail.png'); ?>
    <?php __imgBoxCss('ico_facebook.png'); ?>
    <?php __imgBoxCss('ico_world.png'); ?>
    <?php __imgBoxCss('ico_youtube.png'); ?>
    <?php __imgBoxCss('payments_methods.png'); ?>

</style>

<table class="main_table" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <div class="body">
                <div class="header_bar">
                    <div class="wrapper">
                        <div class="left"><?php __e('headbar_1'); ?></div>
                        <div class="right"><a href="<?php __lk('headbar_2');  ?>" target="_blank"><?php __e('headbar_2'); ?></a></div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
</table>