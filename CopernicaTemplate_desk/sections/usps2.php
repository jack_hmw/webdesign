<table cellspacing="0" cellpadding="0" <?= $extra; ?>>
    <tr>
        <td class="blocks" style="padding:0 32px;">
            <table cellspacing="0" cellpadding="0" <?= $extra; ?>>
                <tr>
                    <td class="usps2" valign="middle">
                        <?php _itg('ico_lamp_red.png'); ?> <span><?php __eU('usps4'); ?></span>
                    </td>
                    <td class="usps2" valign="middle">
                        <?php _itg('ico_rewind_red.png'); ?> <span><?php __eU('usps5'); ?></span>
                    </td>
                    <td class="usps2" valign="middle">
                        <?php _itg('ico_garantie_red.png'); ?> <span><?php __eU('usps6'); ?></span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="blocks space_end6 space_empty"></td>
    </tr>
</table>