
<table cellspacing="0" cellpadding="0" <?= $extra; ?>>
    <tr>
        <td class="blocks space_start4"></td>
    </tr>
    <tr>
        <td class="blocks tekstafb">
            <h3 class="blue"><?php __e('afbee_text11'); ?></h3>

            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td class="ugch img" valign="top" width="35%">
                        <a href="#"><?php _itg('prod_img8.jpg'); ?></a>
                    </td>
                    <td class="ugch txt small" width="64%" valign="top">
                        <p class="big"><?php __e('afbee_text12'); ?></p>
                        <p class="separator_h1"></p>
                        <p class="small"><?php __e('afbee_text13'); ?></p>
                        <a href="<?php __lk('block1_txt3');  ?>" class="button_t3"><?php __e('afbee_text14'); ?></a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end2"></td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end6 space_empty"></td>
    </tr>
</table>