<table cellspacing="0" cellpadding="0" style="margin: 16px 0;" <?= $extra; ?> class="desk">
    <tr>
        <td class="header logo">
            <?php _itg('logo_hmw.png'); ?>
        </td>
        <td class="header usps_t1 usp">
            <p><?php _itg('ico_truck.png'); ?></p>
            <p><?php __eU('usps1'); ?></p>
        </td>
        <td class="header usps_t1 usp">
            <p><?php _itg('ico_garantie.png'); ?></p>
            <p><?php __eU('usps2'); ?></p>
        </td>
        <td class="header usps_t1 usp">
            <p><?php _itg('ico_calendar.png'); ?></p>
            <p><?php __eU('usps3'); ?></p>
        </td>
    </tr>
</table>