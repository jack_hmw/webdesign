
<table cellspacing="0" cellpadding="0" <?= $extra; ?>>
    <tr>
        <td class="blocks space_start4"></td>
    </tr>
    <tr>
        <td class="blocks uitlegv">
            <h3 class="blue"><?php __e('uitvideo_text1'); ?></h3>

            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td class="ugch img" valign="top" width="60%">
                        <a href="#"><?php _itg('video_img1.jpg'); ?></a>
                    </td>
                    <td class="ugch txt small" width="39%" valign="top" style="background-color:#DDDDDD;">
                        <p class="big"><?php __e('uitvideo_text2'); ?></p>
                        <p class="separator_h2"></p>
                        <p class="small"><?php __e('uitvideo_text3'); ?></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end1"></td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end6 space_empty"></td>
    </tr>
</table>