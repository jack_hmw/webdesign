<table cellspacing="0" cellpadding="0" <?= $extra; ?>>
    <tr>
        <td class="blocks contacts">
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td class="" valign="top" width="62%">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="ico" valign="middle" width="18%">
                                    <?php _itg('color_review_counter.png'); ?>
                                </td>
                                <td class="txt" valign="middle" width="81%">
                                    <p><?php __e('contacts_text2'); ?><br/><?php __e('contacts_text3'); ?></p>
                                </td>
                            </tr>
                            <tr class="ccts1">
                                <td class="ico" valign="middle" width="18%">
                                    <?php _itg('ico_tel.png'); ?>
                                </td>
                                <td class="txt" valign="middle" width="81%">
                                    <a href="<?php __lk('contacts_text5');  ?>">
                                        <?php __e('contacts_text4'); ?><br/><?php __e('contacts_text5'); ?>
                                    </a>
                                </td>
                            </tr>
                            <tr class="ccts2">
                                <td class="ico" valign="middle" width="18%">
                                    <?php _itg('ico_mail.png'); ?>
                                </td>
                                <td class="txt" valign="middle" width="81%">
                                    <a href="<?php __lk('contacts_text6');  ?>"><?php __e('contacts_text6'); ?></a>
                                </td>
                            </tr>
                            <tr class="scls">
                                <td colspan="2">
                                    <a href="<?php __lk('ico_world');  ?>"><?php _itg('ico_world.png','mr'); ?></a>
                                    <a href="<?php __lk('ico_facebook');  ?>"><?php _itg('ico_facebook.png','mr'); ?></a>
                                    <a href="<?php __lk('ico_youtube');  ?>"><?php _itg('ico_youtube.png'); ?></a>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="" valign="bottom" width="37%" style="text-align:right;">
                        <?php _itg('footer_guy.png'); ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="blocks footer">
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td class="pmth">
                        <?php _itg('payments_methods.png'); ?>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:24px;">
                        <a href="<?php __lk('footer_text1');  ?>"><?php __e('footer_text1'); ?></a>
                        &nbsp;&nbsp;&nbsp;
                        <a href="<?php __lk('footer_text2');  ?>"><?php __e('footer_text2'); ?></a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="blocks space_end1 space_empty"></td>
    </tr>
</table>