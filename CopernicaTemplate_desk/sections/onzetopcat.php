
<table cellspacing="0" cellpadding="0" <?= $extra; ?>>
    <tr>
        <td class="blocks bigphtxt space_start4"></td>
    </tr>
    <tr>
        <td class="blocks onzetopcat ph">
            <h3 class="blue"><?php __e('topcat_txt1'); ?></h3>

            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td class="otc img" width="33%" style="<?php _ibk('topcat1.jpg',true); ?>"
                        valign="bottom" align="center" background="<?php _iurl('topcat1.jpg'); ?>">
                        <p style="padding-bottom: 16px;"><a href="<?php __lk('topcat_txt2');  ?>" class=" button_t3 white"><?php __e('topcat_txt2'); ?></a></p>
                    </td>
                    <td class="otc img" width="33%" style="<?php _ibk('topcat2.jpg',true); ?>"
                        valign="bottom" align="center" background="<?php _iurl('topcat2.jpg'); ?>">
                        <p style="padding-bottom: 16px;"><a href="<?php __lk('topcat_txt3');  ?>" class=" button_t3 white"><?php __e('topcat_txt3'); ?></a></p>
                    </td>
                    <td class="otc img" width="33%" style="<?php _ibk('topcat3.jpg',true); ?>"
                        valign="bottom" align="center" background="<?php _iurl('topcat3.jpg'); ?>">
                        <p style="padding-bottom: 16px;"><a href="<?php __lk('topcat_txt4');  ?>" class=" button_t3 white"><?php __e('topcat_txt4'); ?></a></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end2"></td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end6 space_empty"></td>
    </tr>
</table>