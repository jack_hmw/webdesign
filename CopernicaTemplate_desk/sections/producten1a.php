
<table cellspacing="0" cellpadding="0" <?= $extra; ?>>
    <tr>
        <td class="blocks bigphtxt space_start4"></td>
    </tr>
    <tr>
        <td class="blocks producten1 ph" align="center">
            <h3 class="blue"><?php __e('prod_text01'); ?></h3>

            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td class="pdt img" width="33%">
                        <?php _itg('prod_img1.jpg'); ?>
                    </td>
                    <td class="pdt img" width="33%">
                        <?php _itg('prod_img2.jpg'); ?>
                    </td>
                    <td class="pdt img" width="33%">
                        <?php _itg('prod_img3.jpg'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="pdt" width="33%">
                        <h5><?php __e('prod_text11'); ?></h5>
                        <p class="italic"><?php __e('prod_text12'); ?></p>
                        <p class="small txt"><?php __e('prod_text13'); ?></p>
                        <p class="plus small"><?php _itg('ico_plus_green.png'); ?> <?php __e('prod_text14'); ?></p>
                        <p class="plus small"><?php _itg('ico_plus_green.png'); ?> <?php __e('prod_text15'); ?></p>
                        <p class="stars">
                            <?php _itg('ico_star1_full.png'); ?>
                            <?php _itg('ico_star1_full.png'); ?>
                            <?php _itg('ico_star1_full.png'); ?>
                            <?php _itg('ico_star1_half.png'); ?>
                            <?php _itg('ico_star1_empty.png'); ?>
                        </p>
                        <a href="<?php __lk('prod_text16');  ?>" class="button_t3"><?php __e('prod_text16'); ?></a>
                    </td>
                    <td class="pdt" width="33%">
                        <h5><?php __e('prod_text21'); ?></h5>
                        <p class="italic"><?php __e('prod_text22'); ?></p>
                        <p class="small txt"><?php __e('prod_text23'); ?></p>
                        <p class="plus small"><?php _itg('ico_plus_green.png'); ?> <?php __e('prod_text24'); ?></p>
                        <p class="plus small"><?php _itg('ico_plus_green.png'); ?> <?php __e('prod_text25'); ?></p>
                        <p class="stars">
                            <?php _itg('ico_star1_full.png'); ?>
                            <?php _itg('ico_star1_full.png'); ?>
                            <?php _itg('ico_star1_full.png'); ?>
                            <?php _itg('ico_star1_full.png'); ?>
                            <?php _itg('ico_star1_half.png'); ?>
                        </p>
                        <a href="<?php __lk('prod_text26');  ?>" class="button_t3"><?php __e('prod_text26'); ?></a>
                    </td>
                    <td class="pdt" width="33%">
                        <h5><?php __e('prod_text31'); ?></h5>
                        <p class="italic"><?php __e('prod_text32'); ?></p>
                        <p class="small txt"><?php __e('prod_text33'); ?></p>
                        <p class="plus small"><?php _itg('ico_plus_green.png'); ?> <?php __e('prod_text34'); ?></p>
                        <p class="plus small"><?php _itg('ico_plus_green.png'); ?> <?php __e('prod_text35'); ?></p>
                        <p class="stars">
                            <?php _itg('ico_star1_full.png'); ?>
                            <?php _itg('ico_star1_full.png'); ?>
                            <?php _itg('ico_star1_full.png'); ?>
                            <?php _itg('ico_star1_half.png'); ?>
                            <?php _itg('ico_star1_empty.png'); ?>
                        </p>
                        <a href="<?php __lk('prod_text36');  ?>" class="button_t3"><?php __e('prod_text36'); ?></a>
                    </td>
                </tr>

            </table>
        </td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end2"></td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end6 space_empty"></td>
    </tr>
</table>