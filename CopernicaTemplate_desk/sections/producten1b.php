
<table cellspacing="0" cellpadding="0" <?= $extra; ?>>
    <tr>
        <td class="blocks bigphtxt space_start4"></td>
    </tr>
    <tr>
        <td class="blocks producten1 ph" align="center">
            <h3 class="blue"><?php __e('prod_text01'); ?></h3>

            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td class="pdt img" width="49%">
                        <?php _itg('prod_img4.jpg'); ?>
                    </td>
                    <td class="pdt img" width="49%">
                        <?php _itg('prod_img5.jpg'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="pdt" width="49%">
                        <h5><?php __e('prod_text41'); ?></h5>
                        <p class="italic"><?php __e('prod_text42'); ?></p>
                        <p class="small txt"><?php __e('prod_text23'); ?></p>
                        <p class="plus small"><?php _itg('ico_plus_green.png'); ?> <?php __e('prod_text44'); ?></p>
                        <p class="plus small"><?php _itg('ico_plus_green.png'); ?> <?php __e('prod_text45'); ?></p>
                        <p class="stars">
                            <?php _itg('ico_star1_full.png'); ?>
                            <?php _itg('ico_star1_full.png'); ?>
                            <?php _itg('ico_star1_full.png'); ?>
                            <?php _itg('ico_star1_half.png'); ?>
                            <?php _itg('ico_star1_empty.png'); ?>
                        </p>
                        <a href="<?php __lk('prod_text46');  ?>" class="button_t3"><?php __e('prod_text46'); ?></a>
                    </td>
                    <td class="pdt" width="49%">
                        <h5><?php __e('prod_text51'); ?></h5>
                        <p class="italic"><?php __e('prod_text52'); ?></p>
                        <p class="small txt"><?php __e('prod_text53'); ?></p>
                        <p class="plus small"><?php _itg('ico_plus_green.png'); ?> <?php __e('prod_text54'); ?></p>
                        <p class="plus small"><?php _itg('ico_plus_green.png'); ?> <?php __e('prod_text55'); ?></p>
                        <p class="stars">
                            <?php _itg('ico_star1_full.png'); ?>
                            <?php _itg('ico_star1_full.png'); ?>
                            <?php _itg('ico_star1_full.png'); ?>
                            <?php _itg('ico_star1_half.png'); ?>
                            <?php _itg('ico_star1_empty.png'); ?>
                        </p>
                        <a href="<?php __lk('prod_text56');  ?>" class="button_t3"><?php __e('prod_text56'); ?></a>
                    </td>
                </tr>

            </table>
        </td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end2"></td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end6 space_empty"></td>
    </tr>
</table>