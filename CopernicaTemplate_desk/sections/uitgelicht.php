
<table cellspacing="0" cellpadding="0" <?= $extra; ?>>
    <tr>
        <td class="blocks bigphtxt space_start4"></td>
    </tr>
    <tr>
        <td class="blocks uitgelicht">
            <h3 class="blue"><?php __e('uitgh_text1'); ?></h3>

            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td class="ugch txt" valign="top">
                        <h5><?php __e('uitgh_text21'); ?></h5>
                        <p><?php __e('uitgh_text22'); ?></p>
                        <a href="<?php __lk('uitgh_text23');  ?>" class="button_t3"><?php __e('uitgh_text23'); ?></a>
                    </td>
                    <td class="ugch img">
                        <?php _itg('uitph1.jpg'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="ugch img">
                        <?php _itg('uitph2.jpg'); ?>
                    </td>
                    <td class="ugch txt" valign="top">
                        <h5><?php __e('uitgh_text31'); ?></h5>
                        <p><?php __e('uitgh_text32'); ?></p>
                        <a href="<?php __lk('uitgh_text33');  ?>" class="button_t3"><?php __e('uitgh_text33'); ?></a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end2"></td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end6 space_empty"></td>
    </tr>
</table>