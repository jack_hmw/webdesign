<?php

global $TemplateManagerCfg;
if(!isset($TemplateManagerCfg)) $TemplateManagerCfg=array();
$TemplateManagerCfg['imgToBase64']=true;


ob_start();
include('./index.php');
$message = ob_get_contents();
ob_end_clean();

$to = "somebody@example.com, somebodyelse@example.com";
$subject = "Email subject test";

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type: text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From: <giacomo@hulpmiddelwereld.nl>' . "\r\n";
//$headers .= 'Cc: myboss@example.com' . "\r\n";

mail($to,$subject,$message,$headers);
