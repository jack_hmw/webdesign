<?php

$TemplateManagerCfg = array(
        'lang' => 'nl',
        'imgToBase64' => true
);

include('../inc/TemplateManager.class.php');

?>
<style type="text/css">
    <?php include('./css/common.css'); ?>

    <?php include('./css/style.css'); ?>

    <?php include('./css/mobile.css'); ?>

    <?php __imgBoxCss('logo_hmw.png'); ?>
    <?php __imgBoxCss('ico_truck.png'); ?>
    <?php __imgBoxCss('ico_garantie.png'); ?>
    <?php __imgBoxCss('ico_calendar.png'); ?>
    <?php __imgBoxCss('ico_box.png'); ?>
    <?php __imgBackCss('klok_room.png'); ?>

    <?php __imgBackCss('topcat1.jpg'); ?>
    <?php __imgBackCss('topcat2.jpg'); ?>
    <?php __imgBackCss('topcat3.jpg'); ?>

    <?php __imgBoxCss('ico_lamp_red.png'); ?>
    <?php __imgBoxCss('ico_rewind_red.png'); ?>
    <?php __imgBoxCss('ico_garantie_red.png'); ?>

    <?php __imgBackCss('uitph1.jpg',null,array('setheight'=>true)); ?>
    <?php __imgBackCss('uitph2.jpg',null,array('setheight'=>true)); ?>
    <?php __imgBackCss('video_img1.jpg',null,array('setheight'=>true)); ?>
    <?php __imgBackCss('prod_img8.jpg',null,array('setheight'=>false)); ?>

    <?php __imgBoxCss('prod_img1.jpg'); ?>
    <?php __imgBoxCss('prod_img2.jpg'); ?>
    <?php __imgBoxCss('prod_img3.jpg'); ?>
    <?php __imgBoxCss('prod_img4.jpg'); ?>
    <?php __imgBoxCss('prod_img5.jpg'); ?>
    <?php __imgBoxCss('prod_img6.jpg'); ?>
    <?php __imgBoxCss('prod_img7.jpg'); ?>

    <?php __imgBoxCss('ico_plus_green.png'); ?>
    <?php __imgBoxCss('ico_star1_full.png'); ?>
    <?php __imgBoxCss('ico_star1_half.png'); ?>
    <?php __imgBoxCss('ico_star1_empty.png'); ?>

    <?php __imgBoxCss('footer_guy.png'); ?>
    <?php __imgBoxCss('color_review.png'); ?>
    <?php __imgBoxCss('ico_tel.png'); ?>
    <?php __imgBoxCss('ico_mail.png'); ?>
    <?php __imgBoxCss('ico_facebook.png'); ?>
    <?php __imgBoxCss('ico_world.png'); ?>
    <?php __imgBoxCss('ico_youtube.png'); ?>
    <?php __imgBoxCss('payments_methods.png'); ?>

</style>

<div class="body">
<div class="header_bar">
    <div class="wrapper">
        <div class="left"><?php __e('headbar_1'); ?></div>
        <div class="right"><a href="<?php __lk('headbar_2');  ?>" target="_blank"><?php __e('headbar_2'); ?></a></div>
    </div>
</div>

<div class="wrapper">

    <div class="header">
        <div class="logo"><a href="<?php __lk('logo');  ?>" <?php _ibx('logo_hmw.png'); ?>></a></div>
        <div class="usps usps_t1">
            <div class="usp">
                <p <?php _ibx('ico_truck.png'); ?>></p>
                <p><?php __eU('usps1'); ?></p>
            </div>
            <div class="usp">
                <p <?php _ibx('ico_garantie.png'); ?>></p>
                <p><?php __eU('usps2'); ?></p>
            </div>
            <div class="usp">
                <p <?php _ibx('ico_calendar.png'); ?>></p>
                <p><?php __eU('usps3'); ?></p>
            </div>
        </div>
    </div>

    <div class="menu menu1">
        <a href="<?php __lk('menu1');  ?>"><?php __e('mmenu1'); ?></a>
        <a href="<?php __lk('menu2');  ?>"><?php __e('mmenu2'); ?></a>
        <a href="<?php __lk('menu3');  ?>" class="hidden-xs"><?php __e('mmenu3'); ?></a>
        <a href="<?php __lk('menu4');  ?>" class="red"><?php __e('mmenu4'); ?></a>
    </div>

    <div <?php _ibb('klok_room.png','mainphoto'); ?>>
        <a href="<?php __lk('mainphoto_button1');  ?>" class="button button_t1 white"><?php __e('mph_btn1'); ?></a>
    </div>

    <div class="blocks blocks_t1 bigpadding">
        <h4><?php __e('block1_txt1'); ?></h4>
        <p class="biglineh"><?php __e('block1_txt2'); ?></p>
        <a href="<?php __lk('block1_txt3');  ?>" class="button button_t1"><?php __e('block1_txt3'); ?></a>
    </div>

    <div class="blocks blocks_t1 nopadding-lr" id="topcat">
        <h3 class="blue"><?php __e('topcat_txt1'); ?></h3>
        <div class="blocks columns nopadding">
            <div <?php _ibb('topcat1.jpg','w33'); ?>>
                <a href="<?php __lk('topcat_txt2');  ?>" class="button button_t2 white"><?php __e('topcat_txt2'); ?></a>
            </div>
            <div <?php _ibb('topcat2.jpg','w33'); ?>>
                <a href="<?php __lk('topcat_txt3');  ?>" class="button button_t2 white"><?php __e('topcat_txt3'); ?></a>
            </div>
            <div <?php _ibb('topcat3.jpg','w33'); ?>>
                <a href="<?php __lk('topcat_txt4');  ?>" class="button button_t2 white"><?php __e('topcat_txt4'); ?></a>
            </div>
        </div>
    </div>

    <div class="blocks blocks_t1 nopadding">
        <div class="usps usps_t2">
            <div class="usp"><span <?php _ibx('ico_lamp_red.png'); ?>></span> <span><?php __eU('usps4'); ?></span></div>
            <div class="usp"><span <?php _ibx('ico_rewind_red.png'); ?>></span> <span><?php __eU('usps5'); ?></span></div>
            <div class="usp"><span <?php _ibx('ico_garantie_red.png'); ?>></span> <span><?php __eU('usps6'); ?></span></div>
        </div>
    </div>

    <div class="blocks blocks_t1 nopadding-lr">
        <h3 class="blue"><?php __e('uitgh_text1'); ?></h3>
        <div class="blocks columns nopadding inverted">
            <div <?php _ibb('uitph1.jpg','w50'); ?>></div>
            <div class="w50">
                <div class="text">
                    <h5><?php __e('uitgh_text21'); ?></h5>
                    <p class="biglineh"><?php __e('uitgh_text22'); ?></p>
                    <a href="<?php __lk('uitgh_text23');  ?>" class="button button_t3"><?php __e('uitgh_text23'); ?></a>
                </div>
            </div>
        </div>
        <div class="blocks columns nopadding">
            <div <?php _ibb('uitph2.jpg','w50'); ?>></div>
            <div class="w50">
                <div class="text">
                    <h5><?php __e('uitgh_text31'); ?></h5>
                    <p class="biglineh"><?php __e('uitgh_text32'); ?></p>
                    <a href="<?php __lk('uitgh_text33');  ?>" class="button button_t3"><?php __e('uitgh_text33'); ?></a>
                </div>
            </div>
        </div>
    </div>

    <div class="blocks blocks_t1 producten1">
        <h3 class="blue"><?php __e('prod_text01'); ?></h3>
        <div class="blocks columns nopadding">
            <div class="wp33">
                <p <?php _ibx('prod_img1.jpg'); ?>></p>
                <h5><?php __e('prod_text11'); ?></h5>
                <p class="italic"><?php __e('prod_text12'); ?></p>
                <p class="small"><?php __e('prod_text13'); ?></p>
                <p class="plus small"><span <?php _ibx('ico_plus_green.png'); ?>></span> <?php __e('prod_text14'); ?></p>
                <p class="plus small"><span <?php _ibx('ico_plus_green.png'); ?>></span> <?php __e('prod_text15'); ?></p>
                <p class="stars">
                    <span <?php _ibx('ico_star1_full.png'); ?>></span>
                    <span <?php _ibx('ico_star1_full.png'); ?>></span>
                    <span <?php _ibx('ico_star1_full.png'); ?>></span>
                    <span <?php _ibx('ico_star1_half.png'); ?>></span>
                    <span <?php _ibx('ico_star1_empty.png'); ?>></span>
                </p>
                <a href="<?php __lk('prod_text16');  ?>" class="button button_t2"><?php __e('prod_text16'); ?></a>
            </div>
            <div class="wp33">
                <p <?php _ibx('prod_img2.jpg'); ?>></p>
                <h5><?php __e('prod_text21'); ?></h5>
                <p class="italic"><?php __e('prod_text22'); ?></p>
                <p class="small"><?php __e('prod_text23'); ?></p>
                <p class="plus small"><span <?php _ibx('ico_plus_green.png'); ?>></span> <?php __e('prod_text24'); ?></p>
                <p class="plus small"><span <?php _ibx('ico_plus_green.png'); ?>></span> <?php __e('prod_text25'); ?></p>
                <p class="stars">
                    <span <?php _ibx('ico_star1_full.png'); ?>></span>
                    <span <?php _ibx('ico_star1_full.png'); ?>></span>
                    <span <?php _ibx('ico_star1_full.png'); ?>></span>
                    <span <?php _ibx('ico_star1_full.png'); ?>></span>
                    <span <?php _ibx('ico_star1_half.png'); ?>></span>
                </p>
                <a href="<?php __lk('prod_text26');  ?>" class="button button_t2"><?php __e('prod_text26'); ?></a>
            </div>
            <div class="wp33">
                <p <?php _ibx('prod_img3.jpg'); ?>></p>
                <h5><?php __e('prod_text31'); ?></h5>
                <p class="italic"><?php __e('prod_text32'); ?></p>
                <p class="small"><?php __e('prod_text33'); ?></p>
                <p class="plus small"><span <?php _ibx('ico_plus_green.png'); ?>></span> <?php __e('prod_text34'); ?></p>
                <p class="plus small"><span <?php _ibx('ico_plus_green.png'); ?>></span> <?php __e('prod_text35'); ?></p>
                <p class="stars">
                    <span <?php _ibx('ico_star1_full.png'); ?>></span>
                    <span <?php _ibx('ico_star1_full.png'); ?>></span>
                    <span <?php _ibx('ico_star1_full.png'); ?>></span>
                    <span <?php _ibx('ico_star1_half.png'); ?>></span>
                    <span <?php _ibx('ico_star1_empty.png'); ?>></span>
                </p>
                <a href="<?php __lk('prod_text36');  ?>" class="button button_t2"><?php __e('prod_text36'); ?></a>
            </div>
        </div>
    </div>

    <div class="blocks blocks_t1 producten1">
        <h3 class="blue"><?php __e('prod_text01'); ?></h3>
        <div class="blocks columns nopadding">
            <div class="wp50">
                <p <?php _ibx('prod_img4.jpg'); ?>></p>
                <h5><?php __e('prod_text41'); ?></h5>
                <p class="italic"><?php __e('prod_text42'); ?></p>
                <p class="small"><?php __e('prod_text43'); ?></p>
                <p class="plus small"><span <?php _ibx('ico_plus_green.png'); ?>></span> <?php __e('prod_text44'); ?></p>
                <p class="plus small"><span <?php _ibx('ico_plus_green.png'); ?>></span> <?php __e('prod_text45'); ?></p>
                <p class="stars">
                    <span <?php _ibx('ico_star1_full.png'); ?>></span>
                    <span <?php _ibx('ico_star1_full.png'); ?>></span>
                    <span <?php _ibx('ico_star1_full.png'); ?>></span>
                    <span <?php _ibx('ico_star1_half.png'); ?>></span>
                    <span <?php _ibx('ico_star1_empty.png'); ?>></span>
                </p>
                <a href="<?php __lk('prod_text46');  ?>" class="button button_t2"><?php __e('prod_text46'); ?></a>
            </div>
            <div class="wp50">
                <p <?php _ibx('prod_img5.jpg'); ?>></p>
                <h5><?php __e('prod_text51'); ?></h5>
                <p class="italic"><?php __e('prod_text52'); ?></p>
                <p class="small"><?php __e('prod_text53'); ?></p>
                <p class="plus small"><span <?php _ibx('ico_plus_green.png'); ?>></span> <?php __e('prod_text54'); ?></p>
                <p class="plus small"><span <?php _ibx('ico_plus_green.png'); ?>></span> <?php __e('prod_text55'); ?></p>
                <p class="stars">
                    <span <?php _ibx('ico_star1_full.png'); ?>></span>
                    <span <?php _ibx('ico_star1_full.png'); ?>></span>
                    <span <?php _ibx('ico_star1_full.png'); ?>></span>
                    <span <?php _ibx('ico_star1_full.png'); ?>></span>
                    <span <?php _ibx('ico_star1_half.png'); ?>></span>
                </p>
                <a href="<?php __lk('prod_text56');  ?>" class="button button_t2"><?php __e('prod_text56'); ?></a>
            </div>
        </div>
    </div>

    <div class="blocks blocks_t1 nopadding-lr producten2">
        <h3 class="blue"><?php __e('prod_text01'); ?></h3>
        <p style="margin-bottom:3em;"><?php __e('prod_text61'); ?></p>
        <div class="blocks columns nopadding blocks_p1">
            <div class="wp33 small">
                <p <?php _ibx('prod_img6.jpg'); ?>></p>
                <h5><?php __e('prod_text71'); ?></h5>
                <p><?php __e('prod_text72'); ?></p>
                <div class="absolute">
                    <a class="hidden-xs" href="<?php __lk('prod_text71');  ?>"><?php __e('prod_text62'); ?></a>
                    <a class="button hidden-md" href="<?php __lk('prod_text71');  ?>"><?php __e('prod_text62'); ?></a>
                </div>
            </div>
            <div class="wp33 small grey5back">
                <p <?php _ibx('prod_img6.jpg'); ?>></p>
                <h5><?php __e('prod_text81'); ?></h5>
                <p><?php __e('prod_text82'); ?></p>
                <div class="absolute">
                    <a class="hidden-xs" href="<?php __lk('prod_text81');  ?>"><?php __e('prod_text62'); ?></a>
                    <a class="button hidden-md" href="<?php __lk('prod_text81');  ?>"><?php __e('prod_text62'); ?></a>
                </div>
            </div>
            <div class="wp33 small">
                <p <?php _ibx('prod_img6.jpg'); ?>></p>
                <h5><?php __e('prod_text91'); ?></h5>
                <p><?php __e('prod_text92'); ?></p>
                <div class="absolute">
                    <a class="hidden-xs" href="<?php __lk('prod_text91');  ?>"><?php __e('prod_text62'); ?></a>
                    <a class="button hidden-md" href="<?php __lk('prod_text91');  ?>"><?php __e('prod_text62'); ?></a>
                </div>
            </div>
            <div class="wp33 small">
                <p <?php _ibx('prod_img6.jpg'); ?>></p>
                <h5><?php __e('prod_text101'); ?></h5>
                <p><?php __e('prod_text102'); ?></p>
                <div class="absolute">
                    <a class="hidden-xs" href="<?php __lk('prod_text101');  ?>"><?php __e('prod_text62'); ?></a>
                    <a class="button hidden-md" href="<?php __lk('prod_text101');  ?>"><?php __e('prod_text62'); ?></a>
                </div>
            </div>
            <div class="wp66 small grey2back">
                <div class="blocks columns nopadding grey2back flex_column_center blocks_p2">
                    <div <?php _ibx('prod_img7.jpg','w50'); ?>></div>
                    <div class="w50">
                        <h5><?php __e('prod_text111'); ?></h5>
                        <p><?php __e('prod_text112'); ?></p>
                        <a class="hidden-xs" href="<?php __lk('prod_text111');  ?>"><?php __e('prod_text62'); ?></a>
                        <a class="button hidden-md" href="<?php __lk('prod_text111');  ?>"><?php __e('prod_text62'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="blocks blocks_t1 nopadding-lr">
        <h3 class="blue"><?php __e('uitvideo_text1'); ?></h3>
        <div class="blocks nopadding-lr columns">
            <a href="<?php __lk('block1_txt3');  ?>" <?php _ibb('video_img1.jpg','w66'); ?>></a>
            <div class="wpp33 grey2back" style="text-align: left;">
                <p><?php __e('uitvideo_text2'); ?></p>
                <p class="w50 separator_h2"></p>
                <p class="small"><?php __e('uitvideo_text3'); ?></p>
            </div>
        </div>
    </div>

    <div class="blocks blocks_t1 nopadding-lr tekstafb">
        <h3 class="blue"><?php __e('afbee_text11'); ?></h3>
        <div class="blocks columns nopadding">
            <div <?php _ibb('prod_img8.jpg','w33'); ?>></div>
            <div class="wpp66 nopadding-tb" style="text-align: left;">
                <p><?php __e('afbee_text12'); ?></p>
                <p class="w50 separator_h1"></p>
                <p class="small"><?php __e('afbee_text13'); ?></p>
                <a href="<?php __lk('block1_txt3');  ?>" class="button button_t2"><?php __e('afbee_text14'); ?></a>
            </div>
        </div>
    </div>

    <div class="blocks blocks_t1 nopadding-lr tekstafb">
        <h3 class="blue"><?php __e('afbee_text11'); ?></h3>
        <div class="blocks columns nopadding">
            <div <?php _ibb('prod_img8.jpg','w33'); ?>></div>
            <div class="wpp66" style="text-align: left;">
                <p><?php __e('afbee_text12'); ?></p>
                <p class="w50 separator_h1"></p>
                <p class="small"><?php __e('afbee_text13'); ?></p>
                <a href="<?php __lk('afbee_text14');  ?>" class="button button_t2"><?php __e('afbee_text14'); ?></a>
            </div>
        </div>
    </div>

    <div class="blocks blocks_t1 usps usps_t3">
        <div class="usp">
            <p <?php _ibx('ico_truck.png'); ?>></p>
            <p><?php __eU('usps1'); ?></p>
        </div>
        <div class="usp">
            <p <?php _ibx('ico_garantie.png'); ?>></p>
            <p><?php __eU('usps2'); ?></p>
        </div>
        <div class="usp">
            <p <?php _ibx('ico_calendar.png'); ?>></p>
            <p><?php __eU('usps3'); ?></p>
        </div>
        <div class="usp">
            <p <?php _ibx('ico_box.png'); ?>></p>
            <p><?php __eU('usps7'); ?></p>
        </div>
    </div>

    <div class="blocks blocks_t1 columns " id="contacts">
        <div class="wp66" style="text-align: left;">
            <div class="mb ma">
                <span <?php _ibx('color_review.png','review_count ico_left'); ?>><?php __e('contacts_text1'); ?></span>
                <p><?php __e('contacts_text2'); ?><br/><?php __e('contacts_text3'); ?></p>
            </div>

            <a href="<?php __lk('contacts_text5');  ?>" class="ma">
                <span <?php _ibx('ico_tel.png',' ico_left'); ?>></span>
                <p><?php __e('contacts_text4'); ?><br/><?php __e('contacts_text5'); ?></p>
            </a>

            <a href="<?php __lk('contacts_text6');  ?>" class="ma mb">
                <span <?php _ibx('ico_mail.png',' ico_left'); ?>></span>
                <p><?php __e('contacts_text6'); ?></p>
            </a>

            <div>
                <a href="<?php __lk('ico_world');  ?>" <?php _ibx('ico_world.png','mr'); ?>></a>
                <a href="<?php __lk('ico_facebook');  ?>" <?php _ibx('ico_facebook.png','mr'); ?>></a>
                <a href="<?php __lk('ico_youtube');  ?>" <?php _ibx('ico_youtube.png'); ?>></a>
            </div>
        </div>
        <div <?php _ibx('footer_guy.png','w33'); ?> style="align-self:flex-end; margin-left:auto; margin-right:auto;"></div>
    </div>

    <div class="blocks blocks_t1" id="footer">
        <p <?php _ibx('payments_methods.png',''); ?>></p>
        <p>
            <a href="<?php __lk('footer_text1');  ?>"><?php __e('footer_text1'); ?></a>
            &nbsp; &nbsp; &nbsp;
            <a href="<?php __lk('footer_text2');  ?>"><?php __e('footer_text2'); ?></a>
        </p>
    </div>

</div>

</div>