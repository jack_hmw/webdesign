<?php

return array(
    'headbar_1' => 'Bekijk hier de webversie',
    'headbar_2' => 'Preview van de snippet tekst hier',

    'usps1' => 'Gratis levering vanaf € 50,-',
    'usps2' => 'Geld terug garantie',
    'usps3' => '30 dagen bedenktijd',
    'usps4' => 'Gratis advies',
    'usps5' => '30 dagen retour',
    'usps6' => 'Niet goed geld terug',
    'usps7' => 'Gratis Retourzending',

    'mmenu1' => 'INCONTINENTIE',
    'mmenu2' => 'NUTRIDRINK',
    'mmenu3' => 'DEMENTIEKLOKKEN',
    'mmenu4' => 'HELE ASSORTIMENT',

    'mph_btn1' => 'UW PERFECTE OPLOSSING?',

    'block1_txt1' => 'Beste Hans',
    'block1_txt2' => 'Als gewaardeerde klant willen we u een persoonlijke aanbieding doen.<br/>
Om uw persoonlijke aanbieding te verzilveren kunt u de code: Hans043 in uw
winkelwagentje invoeren. U krijgt dan €20,- korting op uw bestelling.',
    'block1_txt3' => 'KORTING DIRECT VERZILVEREN',

    'topcat_txt1' => 'ONZE TOPCATEGORIEN',
    'topcat_txt2' => 'SHOP BEDDEN',
    'topcat_txt3' => 'SHOP ROLLATORS',
    'topcat_txt4' => 'SHOP TELEFOONS',

    'uitgh_text1' => 'UITGELICHT',
    'uitgh_text21' => 'Wellness Collectie',
    'uitgh_text22' => 'Wanneer u een fauteuil gaat aanschaffen is uw keuze uiterst belangrijk. Het gaat immers om uw persoonlijke zitcomfort. Bovendien worden de relax- en sta-opfauteuils uit de Fitform Wellness Collectie op uw persoonlijke maat gemaakt.',
    'uitgh_text23' => 'BEKIJK DE WELLNESS COLLECTIE',
    'uitgh_text31' => 'Gemak Collectie',
    'uitgh_text32' => 'Wanneer u een fauteuil gaat aanschaffen is uw keuze uiterst belangrijk. Het gaat immers om uw persoonlijke zitcomfort. Bovendien worden de relax- en sta-opfauteuils uit de Fitform Wellness Collectie op uw persoonlijke maat gemaakt.',
    'uitgh_text33' => 'BEKIJK DE GEMAK COLLECTIE',

    'prod_text01' => 'PRODUCTEN',
    'prod_text11' => 'Rollz Flex Rollator',
    'prod_text12' => 'Binnen & buiten',
    'prod_text13' => 'De Rollz Flex heeft een zeer sterk en stijlvol aluminium frame. De overige onderdelen zijn gemaakt van hard kunststof en zijn mooi afgewerkt tot in het kleinste detail.',
    'prod_text14' => 'Stabiel &amp; Comfortabel',
    'prod_text15' => 'Stabiel &amp; Comfortabel',
    'prod_text16' => 'BEKIJK NU',

    'prod_text21' => 'Rollz Flex Rollator',
    'prod_text22' => 'Binnen & buiten',
    'prod_text23' => 'De Rollz Flex heeft een zeer sterk en stijlvol aluminium frame. De overige onderdelen zijn gemaakt van hard kunststof en zijn mooi afgewerkt tot in het kleinste detail.',
    'prod_text24' => 'Stabiel &amp; Comfortabel',
    'prod_text25' => 'Stabiel &amp; Comfortabel',
    'prod_text26' => 'BEKIJK NU',

    'prod_text31' => 'Rollz Flex Rollator',
    'prod_text32' => 'Binnen & buiten',
    'prod_text33' => 'De Rollz Flex heeft een zeer sterk en stijlvol aluminium frame. De overige onderdelen zijn gemaakt van hard kunststof en zijn mooi afgewerkt tot in het kleinste detail.',
    'prod_text34' => 'Stabiel &amp; Comfortabel',
    'prod_text35' => 'Stabiel &amp; Comfortabel',
    'prod_text36' => 'BEKIJK NU',

    'prod_text41' => 'Fitform Wellness Premium',
    'prod_text42' => '612 Relaxstoel',
    'prod_text43' => 'Deze draaibare Fitform Wellness Premium 612 Wellnessfauteuil is strak en eigentijds vormgegeven. En is daarom uiterst geschikt voor een strakke, moderne woonkamer.',
    'prod_text44' => 'Wetenschappelijk onderbouwd',
    'prod_text45' => '10 jaar garantie',
    'prod_text46' => 'BEKIJK NU',

    'prod_text51' => 'Fitform 580 Elevo',
    'prod_text52' => '580 Sta-op stoel',
    'prod_text53' => 'Deze draaibare Fitform Wellness Premium 612 Wellnessfauteuil is strak en eigentijds vormgegeven. En is daarom uiterst geschikt voor een strakke, moderne woonkamer.',
    'prod_text54' => 'Wetenschappelijk onderbouwd',
    'prod_text55' => '10 jaar garantie',
    'prod_text56' => 'BEKIJK NU',

    'prod_text61' => 'We hebben een selectie aan producten voor u gemaakt.<br/>Wij denken dat u dit wellicht interessant zou kunnen vinden.',
    'prod_text62' => 'BEKIJK NU',

    'prod_text71' => 'Lichtgewicht bestek',
    'prod_text72' => 'Speciaal ontworpen voor mensen die bij voorkeur het bestek als een pen vasthouden',

    'prod_text81' => 'Ultrasone Luchtbevochtiger',
    'prod_text82' => 'De ultrasone luchtbevochtiger is een ideaal hulpmiddel om in huis optimale omstandigheden te creëren voor uw ademhaling!',

    'prod_text91' => 'Lichtgewicht bestek',
    'prod_text92' => 'Speciaal ontworpen voor mensen die bij voorkeur het bestek als een pen vasthouden.',

    'prod_text101' => 'Puck Sleutelkluis',
    'prod_text102' => 'De Puck Keysafe sleutelkluis is bedoeld om uw woningsleutels veilig op te kunnen bergen bij de woning.',

    'prod_text111' => 'Ceres 4 Deluxe Scootmobiel',
    'prod_text112' => 'De Ceres 4 Deluxe Scootmobiel is de ideale scooter voor binnen en buiten gebruik.',

    'uitvideo_text1' => 'UITLEGVIDEO',
    'uitvideo_text2' => '<strong>BBrain</strong><br/>Slimme Kalenderklok',
    'uitvideo_text3' => 'Bekijk de video en zie de werking van de verschillende functionaliteiten die de klok u zou kunnen bieden.',

    'afbee_text11' => 'TEKST & AFBEELDING',
    'afbee_text12' => '<strong>Op maat betekend ook &eacute;cht op maat</strong><br/>U bent de regisseur',
    'afbee_text13' => 'Bij Hulpmiddelwereld zijn veel producten maatproducten. Dat wil zeggen dat het product voor u op maat gemaakt wordt, zodat het écht bij u past.<br/>Maar dat betekend ook dat u zelf bepaalt hoe het eruit ziet. Zo heeft u bijvoorbeeld controle over het materiaal en de kleur van uw product.',
    'afbee_text14' => 'MEER INFORMATIE',


    'contacts_text1' => '<span class="c">8,<span class="d">6</span></span>',
    'contacts_text2' => '<span class="italic">6115 beoordelingen</span>',
    'contacts_text3' => '<strong>Beoordeling van onze klanten</strong>',
    'contacts_text4' => '<span class="big">Bel 088 - 0080 100</span>',
    'contacts_text5' => '<span class="small">(ma t/m vr: 08:00 - 17:30) </span>',
    'contacts_text6' => '<span class="big">info@hulpmiddelwereld.com</span>',

    'footer_text1' => 'Algemene voorwaarden',
    'footer_text2' => 'Afmelden',

);








