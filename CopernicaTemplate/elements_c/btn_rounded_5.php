<?php
$cfg = array(
    'background-color' =>   (isset($btn_css['background-color'])?   $btn_css['background-color']:$globalCss['base_color3']),
    'border-color' =>       (isset($btn_css['border-color'])?       $btn_css['border-color']:$globalCss['base_color3']),
    'border-radius' =>      (isset($btn_css['border-radius'])?      $btn_css['border-radius']:5),
    'color' =>              (isset($btn_css['color'])?              $btn_css['color']:$globalCss['base_color3']),
    'width' =>              (isset($btn_css['width'])?              $btn_css['width']:'150px'),
    'height' =>             (isset($btn_css['height'])?             $btn_css['height']:'36px'),
    'line-height' =>        (isset($btn_css['line-height'])?        $btn_css['line-height']:'36px')
);
?>
<span href="<?= $content['href']; ?>" class="btn_element btn_rounded_5" style="border:1px solid <?= $cfg['border-color']; ?>; width:<?= $cfg['width'];
?>; line-height:<?= $cfg['height']; ?>; border-radius:<?= $cfg['border-radius']; ?>px; color:<?= $cfg['color'];
?>; display: block; text-align:center; text-decoration:none; -webkit-text-size-adjust:none; mso-hide:all;"><?= $content['text']; /*&rarr;*/ ?></span>