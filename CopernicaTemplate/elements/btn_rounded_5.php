<?php
$cfg = array(
    'background-color' =>   (isset($btn_css['background-color'])?   $btn_css['background-color']:$globalCss['base_color3']),
    'border-color' =>       (isset($btn_css['border-color'])?       $btn_css['border-color']:$globalCss['base_color3']),
    'border-radius' =>      (isset($btn_css['border-radius'])?      $btn_css['border-radius']:8),
    'color' =>              (isset($btn_css['color'])?              $btn_css['color']:$globalCss['base_color3']),
    'font-size' =>          (isset($btn_css['font-size'])?          $btn_css['font-size']:'13px'),
    'width' =>              (isset($btn_css['width'])?              $btn_css['width']:'150px'),
    'height' =>             (isset($btn_css['height'])?             $btn_css['height']:'36px'),
    'line-height' =>        (isset($btn_css['line-height'])?        $btn_css['line-height']:'36px')
);

$cfg['background-color']='none';

$width = floatval($cfg['width']);
$height = floatval($cfg['height']);
$padding = intval($height/3);

$Base_Style  = "text-align:center; text-decoration:none; -webkit-text-size-adjust:none; ";
$Base_Style .= "color: ".$cfg['color'].";";

$TD_Style  = ""; //width: ".$cfg['width'].";";
$TD_Style .= "border-radius: ".$cfg['border-radius']."px;";
$TD_Style .= "display:block !important;";
$TD_Style .= "background: ".$cfg['background-color'].";";
$TD_Style .= "border:1px solid ".$cfg['border-color'].";";
$TD_Style .= "padding: ".$padding."px 0;";

$A_Style  = "display: inline; vertical-align: middle;";
$A_Style .= "font-size: ".$cfg['font-size']." !important;";
$A_Style .= "border-radius: ".$cfg['border-radius']."px;";
//$A_Style  .= "  ";

$A_Text_Style  = "color: ".$cfg['color']." !important; text-decoration:none !important; font-size: ".$cfg['font-size']." !important; ";

?>

<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" width="<?= $width; ?>" class="center-on-narrow" style="margin:0 auto !important; <?php echo" width: ".$cfg['width'].";"; ?>">
    <tbody>
        <tr>
            <td class="button-td" valign="center" style="<?= $Base_Style . $TD_Style; ?>" >
                <a style="<?= $Base_Style . $A_Style; ?>" target="_blank" href="#"><span style="<?= $A_Text_Style; ?>" class="button-link"><?= $content['text']; /*&rarr;*/ ?></span></a>
            </td>
        </tr>
    </tbody>
</table>