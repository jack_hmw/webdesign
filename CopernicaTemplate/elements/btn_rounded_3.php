<?php
$cfg = array(
    'background-color' =>   (isset($btn_css['background-color'])?   $btn_css['background-color']:$globalCss['base_color3']),
    'border-color' =>       (isset($btn_css['border-color'])?       $btn_css['border-color']:$globalCss['base_color3']),
    'border-radius' =>      (isset($btn_css['border-radius'])?      $btn_css['border-radius']:5),
    'color' =>              (isset($btn_css['color'])?              $btn_css['color']:$globalCss['base_color3']),
    'font-size' =>          (isset($btn_css['font-size'])?          $btn_css['font-size']:'12px'),
    'text-align' =>         (isset($btn_css['text-align'])?         $btn_css['text-align']:'center'),
    'width' =>              (isset($btn_css['width'])?              $btn_css['width']:'150px'),
    'height' =>             (isset($btn_css['height'])?             $btn_css['height']:'36px'),
    'line-height' =>        (isset($btn_css['line-height'])?        $btn_css['line-height']:'36px')
);

$cfg['width_td'] = '98%';

$padding_top = 0;
$padding_bottom = 0;
$padding_left = 0;
$padding_right = 0;

$cfg['background-color']='none';

$width = floatval($cfg['width']);
$height = floatval($cfg['height']);
$padding_top = intval($height/3); $padding_bottom=$padding_top;

if($cfg['text-align']=='left'){
    $padding_left=16;
    if(stripos($cfg['width'],'px')>0) $width-=$padding_left;
}else if($cfg['text-align']=='right'){
    $padding_right=8;
    if(stripos($cfg['width'],'px')>0) $width-=$padding_right;
}
if($cfg['text-align']!=='center'){
    if(stripos($cfg['width'],'px')>0) $cfg['width_td']=$width.'px';
    if(stripos($cfg['width'],'%')>0) $cfg['width_td']=($width-7).'%';
}

$Base_Style  = "text-align:".$cfg['text-align']."; text-decoration:none; -webkit-text-size-adjust:none; ";
$Base_Style .= "color: ".$cfg['color'].";";

$TD_Style  = "width: ".$cfg['width_td'].";";
$TD_Style .= "border-radius: ".$cfg['border-radius']."px;";
$TD_Style .= "display:block !important;";
$TD_Style .= "background: ".$cfg['background-color'].";";
$TD_Style .= "border:1px solid ".$cfg['border-color'].";";
$TD_Style .= "padding: ".$padding_top."px ".$padding_right."px ".$padding_bottom."px ".$padding_left."px;";

$A_Style  = "display: inline; vertical-align: middle;";
$A_Style .= "font-size: ".$cfg['font-size']." !important; ";
$A_Style .= "border-radius: ".$cfg['border-radius']."px;";
//$A_Style  .= "  ";

$A_Text_Style  = "color: ".$cfg['color']." !important; text-decoration:none !important; font-size: ".$cfg['font-size']." !important; ";

?>

<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" width="<?= $cfg['width']; ?>" class="center-on-narrow" style="margin:0 auto !important; <?php echo" width: ".$cfg['width'].";"; ?>">
    <tbody>
    <tr>
        <td class="button-td" width="<?= $cfg['width_td']; ?>" valign="center" align="<?= $cfg['text-align']; ?>" style="<?= $Base_Style . $TD_Style; ?>" >
            <a style="<?= $Base_Style . $A_Style; ?>" target="_blank" href="#"><span style="<?= $A_Text_Style; ?>" class="button-link"><?= $content['text']; /*&rarr;*/ ?></span></a>
        </td>
    </tr>
    </tbody>
</table>