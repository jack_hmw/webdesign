<!DOCTYPE html>
<html lang="nl" style="margin:0 auto !important;padding:0 !important;height:100% !important;width:100% !important;">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting">
    <!--link rel="shortcut icon" href="https://www.Klantnaam.nl/bikeimages/favicon.ico"-->
    <title>Hulpmiddelwereld Newsletter</title>
    <!--[if mso]>
    <style>
        * {
            font-family: arial, helvetica !important;
        }
    </style>
    <![endif]-->
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <style>
        a:hover{color:#aaaaaa}
    </style>
    <style type="text/css">
        @media (min-width: 690px) {
            .mobile-only { display:none !important; }
        }
        @media (max-width: 689px) {
            .rspmainct { width:320px !important; }
            .rspcell33_ct { width: 320px !important; }
            .rspcell33{ display: block !important; width:320px !important; }
            .rspcell33p{ display: block !important; width:287px !important; }
            .rspcell40{ display: block !important; width:320px !important; }
            .rspcell40p{ display: block !important; width:287px !important; }
            .rspcell50{ display: block !important; }
            .rspcell50p{ display: block !important; }
            .rspcell60{ display: block !important; width:320px !important; }
            .rspcell60p{ display: block !important; width:287px !important; }
            .rspcell66{ display: block !important; width:320px !important; }
            .rspcell66p{ display: block !important; width:287px !important; }
            .desktop-only { display:none !important; }
        }
        /*table, tr, td, tbody, thead, th, tfoot {*/
        *[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important
        }
        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
            .email-container {
                min-width: 375px !important
            }
        }
        /* Separator */
        /* Header Bar */
        /* Menu1 */
        /* Buttons */
        /* OnzeTopCat */
        /*.onzetopcat .img img { width:100%; height:auto; }*/
        /*.onzetopcat .img img { width:100%; height:auto; }*/
        /* Usps2 */
        /* Usps3 */
        /* Uitgelicht */
        /* Producten1 */
        /* Producten2 */
        /* Uitlegv */
        /* Tekstafb */
        @media (max-width: 689px) {
            .header_bar table {
                width:300px !important;
            }
            .header_bar .left, .header_bar .right {
                text-align: center !important;
                width:100% !important;
            }
            .header {
                padding:8px 0 !important;
            }
            .header.logo {
                width:100% !important;
            }
            .header.usps {
                width:100% !important;
            }
            .menu1 {
                display: block !important;
            }
            .bigphtxt.txt {
                padding: 24px 16px !important;
            }
            .bigphtxt.txt p {
                line-height: 24px !important;
            }
            .button_t4 {
                padding: 6px 12px !important;
                color: #62B3E0 !important;
                border: 1px solid #6CB9E3 !important;
                border-radius: 5px !important;
                text-decoration: none !important;
                letter-spacing: 0.6px !important;
                font-size: 12px !important;
            }
            .producten2 .pdt .btn { }
            .usps2 {
                display: block !important;
                padding:8px 0 !important;
            }
            .usps3 {
                display: block !important;
                padding: 4px 0 !important;
                text-align: left !important;
            }
            .usps3 p { display:inline-block !important; margin-left:8px !important; }
            .tekstafb .ugch.txt { padding-top:32px !important; }
            .tekstafb .ugch.img img { }
            .contacts .rspcell33p { display:none !important; }
            .contacts .tt { margin-left:0px !important; }
            .producten1 .rspcell33p { padding-top:16px !important; padding-bottom:16px !important; border-bottom:1px solid #DDDDDD; }
            .producten2 .rspcell33p { background-color:transparent !important; border-bottom:1px solid #DDDDDD !important; }
            .producten2 .rspcell33p.grey1 { border-bottom:none !important; padding-bottom:0 !important; }
            .producten1 .rspcell50p { padding-top:16px !important; padding-bottom:16px !important; border-bottom:1px solid #DDDDDD !important; }
            .uitgelicht .mbo-td { display:block !important; }
            .uitgelicht .dko-td { display:none !important; }
        }
    </style>
</head>
<body style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;margin:0 auto !important;padding:0 !important;height:100% !important;width:100% !important;">
<table summary="" cellspacing="0" cellpadding="0" border="0" width="100%" style="width:100%;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;border-spacing:0 !important;table-layout:fixed !important;margin:0 auto !important;">
    <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
        <td class="body header_bar" align="center" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;margin:0;padding:0;background-color:#0055AA;padding:10px 20px;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
            <table cellspacing="0" cellpadding="0" style="width:100%;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;border-spacing:0 !important;table-layout:fixed !important;margin:0 auto !important;">
                <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                    <td class="rspcell50  left" width="319" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;width:319px;color:#FFFFFF;font-size:11px;width:50%;text-align:left;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                        Bekijk hier de webversie        </td>
                    <td class="rspcell50  right" width="319" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;width:319px;color:#FFFFFF;font-size:11px;width:50%;text-align:right;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                        <a href="#" target="_blank" style='color:#62B3E0;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#aaaaaa;color:#FFFFFF;font-size:11px;'>Preview van de snippet tekst hier</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
        <td align="center" class="body" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;margin:0;padding:0;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;background-color:#EEEEEE;'>
            <table summary="" cellspacing="0" cellpadding="0" border="0" class="rspmainct " width="640" style="width:100%;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;width:640px;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;border-spacing:0 !important;table-layout:fixed !important;margin:0 auto !important;">
                <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                    <td class="rspmainct " width="640" align="center" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;width:640px;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                        <table cellspacing="0" cellpadding="0" style="width:100%;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;table-layout:auto;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;border-spacing:0 !important;table-layout:fixed !important;margin:0 auto !important;margin: 16px 0;">
                            <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                <td class="rspcell50 header logo" width="319" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;width:319px;letter-spacing:0.3px;font-size:11px;line-height:16px;color:#666666;padding:16px 0;width:40%;text-align:center;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                                    <img src="./images/logo_hmw.png" alt="" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;vertical-align:middle;-ms-interpolation-mode:bicubic;"/>
                                </td>
                                <td class="rspcell50 header usps" width="319" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;width:319px;letter-spacing:0.3px;font-size:11px;line-height:16px;color:#666666;padding:16px 0;width:59%;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                                    <table cellspacing="0" cellpadding="0" style="width:100%;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;table-layout:auto;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;border-spacing:0 !important;table-layout:fixed !important;margin:0 auto !important;width:100%;">
                                        <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                            <td class="usps_t1 usp" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                                                <p style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;margin:5px 0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;margin:0;text-align:center;padding:0 8px;font-size:11px;'><img src="./images/ico_truck.png" alt="" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;vertical-align:middle;-ms-interpolation-mode:bicubic;"/>
                                                </p>
                                                <p style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;margin:5px 0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;margin:0;text-align:center;padding:0 8px;font-size:11px;'>GRATIS LEVERING VANAF € 50,-</p>
                                            </td>
                                            <td class="usps_t1 usp" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                                                <p style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;margin:5px 0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;margin:0;text-align:center;padding:0 8px;font-size:11px;'><img src="./images/ico_garantie.png" alt="" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;vertical-align:middle;-ms-interpolation-mode:bicubic;"/>
                                                </p>
                                                <p style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;margin:5px 0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;margin:0;text-align:center;padding:0 8px;font-size:11px;'>GELD TERUG GARANTIE</p>
                                            </td>
                                            <td class="usps_t1 usp" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                                                <p style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;margin:5px 0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;margin:0;text-align:center;padding:0 8px;font-size:11px;'><img src="./images/ico_calendar.png" alt="" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;vertical-align:middle;-ms-interpolation-mode:bicubic;"/>
                                                </p>
                                                <p style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;margin:5px 0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;margin:0;text-align:center;padding:0 8px;font-size:11px;'>30 DAGEN BEDENKTIJD</p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table cellspacing="0" cellpadding="0" style="width:100%;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;table-layout:auto;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;border-spacing:0 !important;table-layout:fixed !important;margin:0 auto !important;">
                            <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                <td class="menu1" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#0055aa;color:#FFFFFF;font-size:11px;text-align:center;padding:8px 0;letter-spacing:1px;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                                    <a href="#" style='color:#62B3E0;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#aaaaaa;font-size:11px;color:#FFFFFF;text-decoration:none;'>INCONTINENTIE</a>
                                </td>
                                <td class="menu1" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#0055aa;color:#FFFFFF;font-size:11px;text-align:center;padding:8px 0;letter-spacing:1px;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                                    <a href="#" style='color:#62B3E0;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#aaaaaa;font-size:11px;color:#FFFFFF;text-decoration:none;'>NUTRIDRINK</a>
                                </td>
                                <td class="menu1" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#0055aa;color:#FFFFFF;font-size:11px;text-align:center;padding:8px 0;letter-spacing:1px;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                                    <a href="#" style='color:#62B3E0;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#aaaaaa;font-size:11px;color:#FFFFFF;text-decoration:none;'>DEMENTIEKLOKKEN</a>
                                </td>
                                <td class="menu1 red" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#0055aa;color:#FFFFFF;font-size:11px;text-align:center;padding:8px 0;letter-spacing:1px;background-color:#ED6A65;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                                    <a href="#" style='color:#62B3E0;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#aaaaaa;font-size:11px;color:#FFFFFF;text-decoration:none;'>HELE ASSORTIMENT</a>
                                </td>
                            </tr>
                        </table>
                        <table cellspacing="0" cellpadding="0" style="width:100%;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;table-layout:auto;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;border-spacing:0 !important;table-layout:fixed !important;margin:0 auto !important;">
                            <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                <td class="blocks bigphtxt ph" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#FFFFFF;text-align:center;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                                    <a href="#" style='color:#62B3E0;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#aaaaaa;'><img src="./images/klok_room.png" alt="" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;vertical-align:middle;-ms-interpolation-mode:bicubic;width:100%;height:auto;"/>
                                    </a>
                                </td>
                            </tr>
                            <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                <td class="blocks section1" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#FFFFFF;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;padding-top:24px;' align="center">
                                    <table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" width="295" class="center-on-narrow" style="width:100%;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;table-layout:auto;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;border-spacing:0 !important;table-layout:fixed !important;margin:0 auto !important;margin:0 auto !important;  width: 295px;">
                                        <tbody style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                        <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                            <td class="button-td" width="295" valign="center" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;transition:all 100ms ease-in;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;text-align:center; text-decoration:none; -webkit-text-size-adjust:none; font-weight: bold; color: #62B3E0;width: 295px;border-radius: 8px;display:block !important;background: none;border:1px solid #62B3E0;padding: 16px 0;'>
                                                <a style='color:#62B3E0;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#aaaaaa;text-align:center; text-decoration:none; -webkit-text-size-adjust:none; font-weight: bold; color: #62B3E0;display: inline; vertical-align: middle;border-radius: 8px;' target="_blank" href="#"><span style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;text-decoration:none !important;color: #62B3E0 !important; font-weight: bold; text-decoration:none !important; ;' class="button-link">UW PERFECTE OPLOSSING?</span></a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                <td class="blocks bigphtxt space_end2" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#FFFFFF;text-align:center;height:48px;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'></td>
                            </tr>
                            <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                <td class="blocks bigphtxt space_end6 space_empty" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#FFFFFF;text-align:center;height:8px;background-color:transparent;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'></td>
                            </tr>
                            <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                <td class="blocks bigphtxt txt" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#FFFFFF;text-align:center;padding:24px 64px;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                                    <h4 style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;font-size:16px;font-weight:600;margin:24px 0;">Beste Hans</h4>
                                    <p style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;margin:5px 0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;line-height:28px;'>Als gewaardeerde klant willen we u een persoonlijke aanbieding doen.<br style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;"/>
                                        Om uw persoonlijke aanbieding te verzilveren kunt u de code: Hans043 in uw
                                        winkelwagentje invoeren. U krijgt dan €20,- korting op uw bestelling.</p>
                                </td>
                            </tr>
                            <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                <td class="blocks bigphtxt space_end1" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#FFFFFF;text-align:center;height:64px;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'></td>
                            </tr>
                            <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                <td class="blocks bigphtxt space_end6 space_empty" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#FFFFFF;text-align:center;height:8px;background-color:transparent;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'></td>
                            </tr>
                        </table>
                        <table cellspacing="0" cellpadding="0" style="width:100%;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;table-layout:auto;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;border-spacing:0 !important;table-layout:fixed !important;margin:0 auto !important;">
                            <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                <td class="blocks bigphtxt space_start4" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#FFFFFF;text-align:center;height:24px;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'></td>
                            </tr>
                            <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                <td class="blocks onzetopcat ph" align="center" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#FFFFFF;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                                    <h3 class="blue" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;font-size:20px;font-weight:600;margin:36px 0;color:#62b3e0;font-weight:normal;letter-spacing:4px;text-align:center;">ONZE TOPCATEGORIEN</h3>
                                    <table cellspacing="0" cellpadding="0" class="rspcell33_ct" style="width:100%;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;table-layout:auto;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;border-spacing:0 !important;table-layout:fixed !important;margin:0 auto !important;margin: 0 auto;">
                                        <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                            <td class="rspcell33 " width="213" valign="top" align="center" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;width:210px;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                                                <table cellspacing="0" cellpadding="0" class="rspcell33_ct" style="width:100%;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;table-layout:auto;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;border-spacing:0 !important;table-layout:fixed !important;margin:0 auto !important;margin: 0 auto;">
                                                    <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                                        <td class="img" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;text-align:center;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                                                            <a href="#" style='color:#62B3E0;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#aaaaaa;'><img src="./images/topcat1.jpg" alt="" width="213" height="338" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;vertical-align:middle;-ms-interpolation-mode:bicubic;"/>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                                        <td class="btn" align="center" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;padding-top:8px;padding-bottom:32px;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                                                            <table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" width="90" class="center-on-narrow" style="width:100%;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;table-layout:auto;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;border-spacing:0 !important;table-layout:fixed !important;margin:0 auto !important;margin:0 auto !important;  width: 90%;">
                                                                <tbody style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                                                <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                                                    <td class="button-td" width="90" valign="center" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;transition:all 100ms ease-in;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;text-align:center; text-decoration:none; -webkit-text-size-adjust:none; color: #FFFFFF;width: 100%; border-radius: 8px;display:block !important;background: #62B3E0;padding: 7px 0;'>
                                                                        <a style='color:#62B3E0;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#aaaaaa;text-align:center; text-decoration:none; -webkit-text-size-adjust:none; color: #FFFFFF;display: inline; vertical-align: middle; border-radius: 8px; ;' target="_blank" href="#"><span style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;text-decoration:none !important;color: #FFFFFF !important; text-decoration:none !important; font-size: 12px !important; ;' class="button-link">SHOP BEDDEN</span></a>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="rspcell33 img" width="213" valign="top" align="center" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;width:210px;text-align:center;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                                                <table cellspacing="0" cellpadding="0" class="rspcell33_ct" style="width:100%;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;table-layout:auto;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;border-spacing:0 !important;table-layout:fixed !important;margin:0 auto !important;margin: 0 auto;">
                                                    <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                                        <td class="img" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;text-align:center;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                                                            <a href="#" style='color:#62B3E0;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#aaaaaa;'><img src="./images/topcat2.jpg" alt="" width="213" height="338" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;vertical-align:middle;-ms-interpolation-mode:bicubic;"/>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                                        <td class="btn" align="center" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;padding-top:8px;padding-bottom:32px;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                                                            <table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" width="90" class="center-on-narrow" style="width:100%;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;table-layout:auto;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;border-spacing:0 !important;table-layout:fixed !important;margin:0 auto !important;margin:0 auto !important;  width: 90%;">
                                                                <tbody style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                                                <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                                                    <td class="button-td" width="90" valign="center" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;transition:all 100ms ease-in;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;text-align:center; text-decoration:none; -webkit-text-size-adjust:none; color: #FFFFFF;width: 100%; border-radius: 8px;display:block !important;background: #62B3E0;padding: 7px 0;'>
                                                                        <a style='color:#62B3E0;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#aaaaaa;text-align:center; text-decoration:none; -webkit-text-size-adjust:none; color: #FFFFFF;display: inline; vertical-align: middle; border-radius: 8px; ;' target="_blank" href="#"><span style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;text-decoration:none !important;color: #FFFFFF !important; text-decoration:none !important; font-size: 12px !important; ;' class="button-link">SHOP ROLLATORS</span></a>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="rspcell33 img" width="213" valign="top" align="center" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;width:210px;text-align:center;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                                                <table cellspacing="0" cellpadding="0" class="rspcell33_ct" style="width:100%;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;table-layout:auto;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;border-spacing:0 !important;table-layout:fixed !important;margin:0 auto !important;margin: 0 auto;">
                                                    <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                                        <td class="img" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;text-align:center;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                                                            <a href="#" style='color:#62B3E0;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#aaaaaa;'><img src="./images/topcat3.jpg" alt="" width="213" height="338" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;vertical-align:middle;-ms-interpolation-mode:bicubic;"/>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                                        <td class="btn" align="center" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;padding-top:8px;padding-bottom:32px;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'>
                                                            <table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" width="90" class="center-on-narrow" style="width:100%;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;table-layout:auto;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;border-spacing:0 !important;table-layout:fixed !important;margin:0 auto !important;margin:0 auto !important;  width: 90%;">
                                                                <tbody style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                                                <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                                                    <td class="button-td" width="90" valign="center" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;transition:all 100ms ease-in;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;text-align:center; text-decoration:none; -webkit-text-size-adjust:none; color: #FFFFFF;width: 100%; border-radius: 8px;display:block !important;background: #62B3E0;padding: 7px 0;'>
                                                                        <a style='color:#62B3E0;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#aaaaaa;text-align:center; text-decoration:none; -webkit-text-size-adjust:none; color: #FFFFFF;display: inline; vertical-align: middle; border-radius: 8px; ;' target="_blank" href="#"><span style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;text-decoration:none !important;color: #FFFFFF !important; text-decoration:none !important; font-size: 12px !important; ;' class="button-link">SHOP TELEFOONS</span></a>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                <td class="blocks bigphtxt space_end6 space_empty" style='color:#444444;font-family:"Open Sans", Arial, Helvetica;font-size:13px;line-height:18px;letter-spacing:0.6px;padding:0;margin:0;border:none;border-collapse:collapse;border-spacing:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#FFFFFF;text-align:center;height:8px;background-color:transparent;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;'></td>
                            </tr>
                        </table>
                        <!---->
                        <!--                        -->
                        <!---->
                        <!--                        -->
                        <!---->
                        <!--                        -->
                        <!---->
                        <!--                        -->
                        <!---->
                        <!--                        -->
                        <!---->
                        <!--                        -->
                        <!---->
                        <!--                        -->
                        <!---->
                        <!--                        -->
                        <!---->
                        <!--                        -->
                        <!---->
                        <!--                        -->
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>