<?php
global $HTML;
?>
<table cellspacing="0" cellpadding="0" <?= $extra; ?>>
    <tr>
        <td class="blocks bigphtxt space_start4"></td>
    </tr>
    <tr>
        <td class="blocks producten2 ph" align="center">
            <h3 class="blue"><?php __e('prod_text01'); ?></h3>

            <table cellspacing="0" cellpadding="0" class="rspcell33_ct">
                <tr>
                    <td <?php _rSC33p("pdt"); ?>>
                        <p class="img"><a href="<?= __lkr('prod_text16'); ?>"><?php _itg('prod_img6.jpg'); ?></a></p>
                        <h5><?php __e('prod_text11'); ?></h5>
                        <p class="small txt"><?php __e('prod_text13'); ?></p>
                        <p class="btn">
                            <?php
                            $HTML->element('btn_rounded_3',array(
                                'text' => __('prod_text16'),
                                'href' => __lkr('prod_text16')
                            ),array(
                                'width'=>'60%',
                                'height'=>'16px',
                            ));
                            ?>
                        </p>
                    </td>
                    <td <?php _rSC33p("pdt grey2"); ?>>
                        <p class="img"><a href="<?= __lkr('prod_text16'); ?>"><?php _itg('prod_img6.jpg'); ?></a></p>
                        <h5><?php __e('prod_text11'); ?></h5>
                        <p class="small txt"><?php __e('prod_text13'); ?></p>
                        <p class="btn">
                            <?php
                            $HTML->element('btn_rounded_3',array(
                                'text' => __('prod_text16'),
                                'href' => __lkr('prod_text16')
                            ),array(
                                'width'=>'60%',
                                'height'=>'16px',
                            ));
                            ?>
                        </p>
                    </td>
                    <td <?php _rSC33p("pdt"); ?>>
                        <p class="img"><a href="<?= __lkr('prod_text16'); ?>"><?php _itg('prod_img6.jpg'); ?></a></p>
                        <h5><?php __e('prod_text11'); ?></h5>
                        <p class="small txt"><?php __e('prod_text13'); ?></p>
                        <p class="btn">
                            <?php
                            $HTML->element('btn_rounded_3',array(
                                'text' => __('prod_text16'),
                                'href' => __lkr('prod_text16')
                            ),array(
                                'width'=>'60%',
                                'height'=>'16px',
                            ));
                            ?>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td <?php _rSC33p("pdt"); ?>>
                        <p class="img"><a href="<?= __lkr('prod_text16'); ?>"><?php _itg('prod_img6.jpg'); ?></a></p>
                        <h5><?php __e('prod_text11'); ?></h5>
                        <p class="small txt"><?php __e('prod_text13'); ?></p>
                        <p class="btn">
                            <?php
                            $HTML->element('btn_rounded_3',array(
                                'text' => __('prod_text16'),
                                'href' => __lkr('prod_text16')
                            ),array(
                                'width'=>'60%',
                                'height'=>'16px',
                            ));
                            ?>
                        </p>
                    </td>
                    <td <?php _rSC33p("pdt grey1"); ?>>
                        <p class="img"><a href="<?= __lkr('prod_text16'); ?>"><?php _itg('prod_img7.jpg'); ?></a></p>
                    <td <?php _rSC33p("pdt grey1"); ?>>
                        <h5><?php __e('prod_text11'); ?></h5>
                        <p class="small txt"><?php __e('prod_text13'); ?></p>
                        <p class="btn">
                            <?php
                            $HTML->element('btn_rounded_3',array(
                                'text' => __('prod_text16'),
                                'href' => __lkr('prod_text16')
                            ),array(
                                'width'=>'60%',
                                'height'=>'16px',
                            ));
                            ?>
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end2"></td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end6 space_empty"></td>
    </tr>
</table>