<table cellspacing="0" cellpadding="0" style="margin: 16px 0;">
    <tr>
        <td <?php _rSC50("header logo"); ?>>
            <?php _itg('logo_hmw.png'); ?>
        </td>
        <td <?php _rSC50("header usps"); ?>>
            <table cellspacing="0" cellpadding="0" style="width:100%;">
                <tr>
                    <td class="usps_t1 usp">
                        <p><?php _itg('ico_truck.png'); ?></p>
                        <p><?php __eU('usps1'); ?></p>
                    </td>
                    <td class="usps_t1 usp">
                        <p><?php _itg('ico_garantie.png'); ?></p>
                        <p><?php __eU('usps2'); ?></p>
                    </td>
                    <td class="usps_t1 usp">
                        <p><?php _itg('ico_calendar.png'); ?></p>
                        <p><?php __eU('usps3'); ?></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>