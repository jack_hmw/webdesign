<?php
global $HTML;
?>
<table cellspacing="0" cellpadding="0" <?= $extra; ?>>
    <tr>
        <td class="blocks bigphtxt space_start4"></td>
    </tr>
    <tr>
        <td class="blocks uitgelicht">
            <h3 class="blue"><?php __e('uitgh_text1'); ?></h3>

            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td <?php _rSC50('ugch img'); ?>>
                        <a href="<?= __lkr('uitgh_text23'); ?>"><?php _itg('uitph1.jpg'); ?></a>
                    </td>
                    <td valign="top" <?php _rSC50p('ugch txt'); ?>>
                        <h5><?php __e('uitgh_text21'); ?></h5>
                        <p><?php __e('uitgh_text22'); ?></p>

                        <?php
                        $HTML->element('btn_rounded_3',array(
                            'text' => __('uitgh_text23'),
                            'href' => __lkr('uitgh_text23')
                        ),array(
                            'width'=>'100%',
                            'text-align' => 'left',
                            'height'=>'16px',
                        ));
                        ?>
                    </td>
                </tr>
                <tr>
                    <td <?php _rSC50('ugch img'); ?>>
                        <a href="<?= __lkr('uitgh_text33'); ?>"><?php _itg('uitph2.jpg'); ?></a>
                    </td>
                    <td valign="top" <?php _rSC50p('ugch txt'); ?>>
                        <h5><?php __e('uitgh_text31'); ?></h5>
                        <p><?php __e('uitgh_text32'); ?></p>

                        <?php
                        $HTML->element('btn_rounded_3',array(
                            'text' => __('uitgh_text33'),
                            'href' => __lkr('uitgh_text33')
                        ),array(
                            'width'=>'100%',
                            'text-align' => 'left',
                            'height'=>'16px',
                        ));
                        ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end4"></td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end6 space_empty"></td>
    </tr>
</table>