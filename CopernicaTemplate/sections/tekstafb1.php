<?php
global $HTML;
?>
<table cellspacing="0" cellpadding="0" <?= $extra; ?>>
    <tr>
        <td class="blocks space_start4"></td>
    </tr>
    <tr>
        <td class="blocks tekstafb">
            <h3 class="blue"><?php __e('afbee_text11'); ?></h3>

            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td <?php _rSC33("ugch img"); ?> valign="top" align="center">
                        <a href="#"><?php _itg('prod_img8.jpg'); ?></a>
                    </td>
                    <td <?php _rSC66p("ugch txt small"); ?> valign="top">
                        <p class="big"><?php __e('afbee_text12'); ?></p>
                        <p class="separator_h1"></p>
                        <p class="small"><?php __e('afbee_text13'); ?></p>
                        <?php
                        $HTML->element('btn_rounded_3',array(
                            'text' => __('afbee_text14'),
                            'href' => __lkr('block1_txt3')
                        ),array(
                            'width'=>'100%',
                            'height'=>'18px',
                        ));
                        ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end2"></td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end6 space_empty"></td>
    </tr>
</table>