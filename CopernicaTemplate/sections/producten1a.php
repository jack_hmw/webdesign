<?php
global $HTML;
?>
<table cellspacing="0" cellpadding="0" <?= $extra; ?>>
    <tr>
        <td class="blocks bigphtxt space_start4"></td>
    </tr>
    <tr>
        <td class="blocks producten1 ph" align="center">
            <h3 class="blue"><?php __e('prod_text01'); ?></h3>

            <table cellspacing="0" cellpadding="0" class="rspcell33_ct">
                <tr>
                    <td <?php _rSC33p(); ?> valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="pdt img">
                                    <a href="<?= __lkr('prod_text16'); ?>"><?php _itg('prod_img1.jpg'); ?></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="pdt">
                                    <h5><?php __e('prod_text11'); ?></h5>
                                    <p class="italic"><?php __e('prod_text12'); ?></p>
                                    <p class="small txt"><?php __e('prod_text13'); ?></p>
                                </td>
                            </tr>
                            <tr>
                                <td class="plus small" valign="top">
                                    <?php _itg('ico_plus_green.png'); ?> <span>&nbsp;<?php __e('prod_text14'); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="plus small" valign="top">
                                    <?php _itg('ico_plus_green.png'); ?> <span>&nbsp;<?php __e('prod_text15'); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="stars" valign="top">
                                    <?php _itg('ico_star1_full.png'); ?>
                                    <?php _itg('ico_star1_full.png'); ?>
                                    <?php _itg('ico_star1_full.png'); ?>
                                    <?php _itg('ico_star1_half.png'); ?>
                                    <?php _itg('ico_star1_empty.png'); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    $HTML->element('btn_rounded_3',array(
                                        'text' => __('prod_text16'),
                                        'href' => __lkr('prod_text16')
                                    ),array(
                                        'width'=>'98%',
                                        'height'=>'16px',
                                    ));
                                    ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td <?php _rSC33p(); ?> valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="pdt img">
                                    <a href="<?= __lkr('prod_text16'); ?>"><?php _itg('prod_img2.jpg'); ?></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="pdt">
                                    <h5><?php __e('prod_text21'); ?></h5>
                                    <p class="italic"><?php __e('prod_text22'); ?></p>
                                    <p class="small txt"><?php __e('prod_text23'); ?></p>
                                </td>
                            </tr>
                            <tr>
                                <td class="plus small" valign="top">
                                    <?php _itg('ico_plus_green.png'); ?> <span>&nbsp;<?php __e('prod_text24'); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="plus small" valign="top">
                                    <?php _itg('ico_plus_green.png'); ?> <span>&nbsp;<?php __e('prod_text25'); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="stars" valign="top">
                                    <?php _itg('ico_star1_full.png'); ?>
                                    <?php _itg('ico_star1_full.png'); ?>
                                    <?php _itg('ico_star1_full.png'); ?>
                                    <?php _itg('ico_star1_full.png'); ?>
                                    <?php _itg('ico_star1_half.png'); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    $HTML->element('btn_rounded_3',array(
                                        'text' => __('prod_text26'),
                                        'href' => __lkr('prod_text26')
                                    ),array(
                                        'width'=>'98%',
                                        'height'=>'16px',
                                    ));
                                    ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td <?php _rSC33p(); ?> valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="pdt img">
                                    <a href="<?= __lkr('prod_text16'); ?>"><?php _itg('prod_img3.jpg'); ?></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="pdt">
                                    <h5><?php __e('prod_text31'); ?></h5>
                                    <p class="italic"><?php __e('prod_text32'); ?></p>
                                    <p class="small txt"><?php __e('prod_text33'); ?></p>
                                </td>
                            </tr>
                            <tr>
                                <td class="plus small" valign="top">
                                    <?php _itg('ico_plus_green.png'); ?> <span>&nbsp;<?php __e('prod_text34'); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="plus small" valign="top">
                                    <?php _itg('ico_plus_green.png'); ?> <span>&nbsp;<?php __e('prod_text35'); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="stars" valign="top">
                                    <?php _itg('ico_star1_full.png'); ?>
                                    <?php _itg('ico_star1_full.png'); ?>
                                    <?php _itg('ico_star1_full.png'); ?>
                                    <?php _itg('ico_star1_half.png'); ?>
                                    <?php _itg('ico_star1_empty.png'); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    $HTML->element('btn_rounded_3',array(
                                        'text' => __('prod_text36'),
                                        'href' => __lkr('prod_text36')
                                    ),array(
                                        'width'=>'98%',
                                        'height'=>'16px',
                                    ));
                                    ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end2"></td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end6 space_empty"></td>
    </tr>
</table>