<!DOCTYPE html>
<html lang="nl">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting">
    <!--link rel="shortcut icon" href="https://www.Klantnaam.nl/bikeimages/favicon.ico"-->
    <title><?php if(isset($extra['title'])) echo $extra['title']; ?></title>

    <!--[if mso]>
    <style>
        * {
            font-family: arial, helvetica !important;
        }
    </style>
    <![endif]-->

    <style type="text/css">

        <?php _responsiveCss(); ?>

        <?php echo "\n"; include('./css/style.css'); ?>

        <?php _includeMobileCss('./css/s300.css'); ?>

    </style>

    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
</head>