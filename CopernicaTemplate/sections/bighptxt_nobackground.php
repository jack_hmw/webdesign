<?php
global $HTML;
?>
<table cellspacing="0" cellpadding="0" <?= $extra; ?>>
    <tr>
        <td class="blocks bigphtxt ph">
            <a href="<?= __lkr('mainphoto_button1'); ?>"><?php _itg('klok_room.png','',640,410); ?></a>
        </td>
    </tr>

    <tr>
        <td class="blocks bigphtxt txt">
            <h4><?php __e('block1_txt1'); ?></h4>
            <p><?php __e('block1_txt2'); ?></p>
        </td>
    </tr>

    <tr>
        <td class="blocks section1" align="center">
            <?php
            $HTML->element('btn_rounded_5',array(
                'text' => __('mph_btn1'),
                'href' => __lkr('mainphoto_button1')
            ),array(
                'width'=>'295px',
                'height'=>'50px',
            ));
            ?>
        </td>
    </tr>

    <tr>
        <td class="blocks bigphtxt space_end2"></td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end6 space_empty"></td>
    </tr>
</table>