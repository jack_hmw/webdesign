<table cellspacing="0" cellpadding="0" <?= $extra; ?>>
    <tr>
        <td class="blocks" style="padding:16px 0;">
            <table cellspacing="0" cellpadding="0" <?= $extra; ?>>
                <tr>
                    <td class="usps3" valign="middle">
                        <?php _itg('ico_truck.png'); ?> <p><?php __eU('usps1'); ?></p>
                    </td>
                    <td class="usps3" valign="middle">
                        <?php _itg('ico_garantie.png'); ?> <p><?php __eU('usps2'); ?></p>
                    </td>
                    <td class="usps3" valign="middle">
                        <?php _itg('ico_calendar.png'); ?> <p><?php __eU('usps3'); ?></p>
                    </td>
                    <td class="usps3" valign="middle">
                        <?php _itg('ico_box.png'); ?> <p><?php __eU('usps7'); ?></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="blocks space_end6 space_empty"></td>
    </tr>
</table>