<table cellspacing="0" cellpadding="0" <?= $extra; ?>>
    <tr>
        <td class="blocks bigphtxt ph" style="<?php _ibk('klok_room.png',true); ?>"
            valign="bottom" background="<?php _iurl('klok_room.png'); ?>" width="640" height="450">

            <!--[if mso ]>
            <v:rect style="width:640px;height:350px;" strokecolor="none" width="640" height="450">
                <v:fill type="tile" color="#363636" src="<?php _iurl('klok_room.png'); ?>" />
                <v:textbox inset="0,0,0,0">
            <![endif]-->

            <table width="640" height="450" border="0" cellpadding="0" cellspacing="0" align="center" style="width:640px; height: 450px;">
                <tr>
                    <td valign="middle" style="vertical-align: middle; text-align: center; width:640px; height: 450px;" width="640" height="450">
                        <p style="margin-bottom: 32px;text-align: center;">
                            <a href="<?php __lk('mainphoto_button1');  ?>" class="button_t1 white" ><?php __e('mph_btn1'); ?></a>
                        </p>
                    </td>
                </tr>
            </table>

            <!--[if mso ]>
                 </v:textbox>
            </v:rect>
            <![endif]-->
        </td>
    </tr>
    <tr>
        <td class="blocks bigphtxt txt">
            <h4><?php __e('block1_txt1'); ?></h4>
            <p><?php __e('block1_txt2'); ?></p>
        </td>
    </tr>
    <tr>
        <td class="blocks bigphtxt btn">
            <a href="<?php __lk('block1_txt3');  ?>" class="button_t1"><?php __e('block1_txt3'); ?></a>
        </td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end1"></td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end6 space_empty"></td>
    </tr>
</table>