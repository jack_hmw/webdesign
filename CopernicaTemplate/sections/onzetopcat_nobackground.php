<?php
global $HTML;
?>
<table cellspacing="0" cellpadding="0" <?= $extra; ?>>
    <tr>
        <td class="blocks bigphtxt space_start4"></td>
    </tr>
    <tr>
        <td class="blocks onzetopcat ph" align="center">
            <h3 class="blue"><?php __e('topcat_txt1'); ?></h3>

            <table cellspacing="0" cellpadding="0" class="rspcell33_ct" style="margin: 0 auto;">
                <tr>
                    <td <?php _rSC33(); ?> valign="top" align="center">
                        <table cellspacing="0" cellpadding="0" class="rspcell33_ct" style="margin: 0 auto;">
                            <tr>
                                <td class="img">
                                    <a href="<?= __lkr('topcat_txt2'); ?>"><?php _itg('topcat1.jpg','',213,337); ?></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="btn" align="center">
                                <?php
                                    $HTML->element('btn_rounded_5',array(
                                        'text' => __('topcat_txt2'),
                                        'href' => __lkr('topcat_txt2')
                                    ),array(
                                        'width'=>'90%',
                                        'height'=>'22px',
                                        'font-size'=>'12px',
                                    ));
                                ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td <?php _rSC33("img"); ?> valign="top" align="center">
                        <table cellspacing="0" cellpadding="0" class="rspcell33_ct" style="margin: 0 auto;">
                            <tr>
                                <td class="img">
                                    <a href="<?= __lkr('topcat_txt3'); ?>"><?php _itg('topcat2.jpg','',213,337); ?></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="btn" align="center">
                                <?php
                                    $HTML->element('btn_rounded_5',array(
                                        'text' => __('topcat_txt3'),
                                        'href' => __lkr('topcat_txt3')
                                    ),array(
                                        'width'=>'90%',
                                        'height'=>'22px',
                                        'font-size'=>'12px',
                                    ));
                                ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td <?php _rSC33("img"); ?> valign="top" align="center">
                        <table cellspacing="0" cellpadding="0" class="rspcell33_ct" style="margin: 0 auto;">
                            <tr>
                                <td class="img">
                                    <a href="<?= __lkr('topcat_txt4'); ?>"><?php _itg('topcat3.jpg','',213,337); ?></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="btn" align="center">
                                <?php
                                    $HTML->element('btn_rounded_5',array(
                                        'text' => __('topcat_txt4'),
                                        'href' => __lkr('topcat_txt4')
                                    ),array(
                                        'width'=>'90%',
                                        'height'=>'22px',
                                        'font-size'=>'12px',
                                    ));
                                ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end6 space_empty"></td>
    </tr>
</table>