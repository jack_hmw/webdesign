<?php
global $HTML;
?>
<table cellspacing="0" cellpadding="0" <?= $extra; ?>>
    <tr>
        <td class="blocks bigphtxt space_start4"></td>
    </tr>
    <tr>
        <td class="blocks producten1 ph" align="center">
            <h3 class="blue"><?php __e('prod_text01'); ?></h3>

            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td <?php _rSC50p(); ?> valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="pdt img">
                                    <a href="<?= __lkr('prod_text16'); ?>"><?php _itg('prod_img4.jpg'); ?></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="pdt">
                                    <h5><?php __e('prod_text41'); ?></h5>
                                    <p class="italic"><?php __e('prod_text42'); ?></p>
                                    <p class="small txt"><?php __e('prod_text23'); ?></p>
                                </td>
                            </tr>
                            <tr>
                                <td class="plus small" valign="top">
                                    <?php _itg('ico_plus_green.png'); ?> <span>&nbsp;<?php __e('prod_text44'); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="plus small" valign="top">
                                    <?php _itg('ico_plus_green.png'); ?> <span>&nbsp;<?php __e('prod_text45'); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="stars" valign="top">
                                    <?php _itg('ico_star1_full.png'); ?>
                                    <?php _itg('ico_star1_full.png'); ?>
                                    <?php _itg('ico_star1_full.png'); ?>
                                    <?php _itg('ico_star1_half.png'); ?>
                                    <?php _itg('ico_star1_empty.png'); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    $HTML->element('btn_rounded_3',array(
                                        'text' => __('prod_text46'),
                                        'href' => __lkr('prod_text46')
                                    ),array(
                                        'width'=>'98%',
                                        'height'=>'16px',
                                    ));
                                    ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td <?php _rSC50p(); ?> valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="pdt img">
                                    <a href="<?= __lkr('prod_text16'); ?>"><?php _itg('prod_img5.jpg'); ?></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="pdt">
                                    <h5><?php __e('prod_text51'); ?></h5>
                                    <p class="italic"><?php __e('prod_text52'); ?></p>
                                    <p class="small txt"><?php __e('prod_text53'); ?></p>
                                </td>
                            </tr>
                            <tr>
                                <td class="plus small" valign="top">
                                    <?php _itg('ico_plus_green.png'); ?> <span>&nbsp;<?php __e('prod_text54'); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="plus small" valign="top">
                                    <?php _itg('ico_plus_green.png'); ?> <span>&nbsp;<?php __e('prod_text55'); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="stars" valign="top">
                                    <?php _itg('ico_star1_full.png'); ?>
                                    <?php _itg('ico_star1_full.png'); ?>
                                    <?php _itg('ico_star1_full.png'); ?>
                                    <?php _itg('ico_star1_half.png'); ?>
                                    <?php _itg('ico_star1_empty.png'); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    $HTML->element('btn_rounded_3',array(
                                        'text' => __('prod_text56'),
                                        'href' => __lkr('prod_text56')
                                    ),array(
                                        'width'=>'98%',
                                        'height'=>'16px',
                                    ));
                                    ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end2"></td>
    </tr>
    <tr>
        <td class="blocks bigphtxt space_end6 space_empty"></td>
    </tr>
</table>