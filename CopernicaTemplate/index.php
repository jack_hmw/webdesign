<?php

global $TemplateManagerCfg;
if(!isset($TemplateManagerCfg)) $TemplateManagerCfg=array();
$TemplateManagerCfg['lang']='nl';
//$TemplateManagerCfg['imgNameOnly']=true;
//$TemplateManagerCfg['imgToBase64']=true;

include('../inc/TemplateManager.class.php');
include('../inc/HTML.class.php');
global $HTML;

?>
<!DOCTYPE html>
<html>
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <style type="text/css">

        <?php include('./css/common_legacy.css'); ?>

    </style>
</head>
<body>
<style type="text/css">
    @media screen and (max-width: 699px) {
        .desk { display:none; }
    }
    @media screen and (min-width: 700px) {
        .mobi { display:none; }
    }
</style>

<table summary=""cellspacing="0" cellpadding="0" class="main_container">
    <tr>
        <td class="body">
            <?php $HTML->section('header_bar') ?>
        </td>
    </tr>
    <tr>
        <td align="center">
            <table summary=""cellspacing="0" cellpadding="0"  style="margin:0 auto;">
                <tr>
                    <td align="center" class="body" style="">

                        <?php $HTML->section('header') ?>

                        <?php $HTML->section('menu1') ?>

                        <?php $HTML->section('bighptxt') ?>

                        <?php $HTML->section('onzetopcat') ?>

                        <?php $HTML->section('usps2') ?>

                        <?php $HTML->section('uitgelicht') ?>

                        <?php $HTML->section('producten1a') ?>

                        <?php $HTML->section('producten1b') ?>

                        <?php $HTML->section('producten1c') ?>

                        <?php $HTML->section('uitlegvideo') ?>

                        <?php $HTML->section('tekstafb1') ?>

                        <?php $HTML->section('tekstafb2') ?>

                        <?php $HTML->section('usps3') ?>

                        <?php $HTML->section('contacts') ?>

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

</body>
</html>




