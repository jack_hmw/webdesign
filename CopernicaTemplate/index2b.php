<?php

$max_width = 640;

?><!DOCTYPE html>
<html>
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <style type="text/css">

        .stdcl{
            width: <?= $max_width/2-1 ?>px;
            float: left;
        }
        .big_col {
            width:100%;
        }

        .main_ct {
            width:<?= $max_width ?>px;
        }


        @media (max-width: <?= $max_width+50-1; ?>px) {
            .main_ct {
                width:<?= $max_width/2 ?>px !important;
            }
        }

    </style>
</head>
<body>

    <table summary=""cellspacing="0" cellpadding="0" border="0" width="100%">
        <tr>
            <td align="center">

                <table summary=""cellspacing="0" cellpadding="0" border="0" class="main_ct">

                    <tr>
                        <td class="main_ct">

                            <table summary=""cellspacing="0" cellpadding="0" border="0" class="big_col"
                                   style="background-color:#153955;">
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>

                            <table summary=""cellspacing="0" cellpadding="0" border="0" width="<?= $max_width/2-1 ?>" class="stdcl" style="background-color: #6cb9e3;">
                                <tr>
                                    <td class="stdcl" width="<?= $max_width/2-1 ?>">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>

                            <table summary=""cellspacing="0" cellpadding="0" border="0" class="stdcl" width="<?= $max_width/2-1 ?>"  style="background-color:#ED6A65;">
                                <tr>
                                    <td class="stdcl" width="<?= $max_width/2-1 ?>">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>

</body>
</html>