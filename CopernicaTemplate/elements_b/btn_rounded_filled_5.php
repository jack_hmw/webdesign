<?php
$cfg = array(
    'background-color' =>   (isset($btn_css['background-color'])?   $btn_css['background-color']:$globalCss['base_color3']),
    'border-color' =>       (isset($btn_css['border-color'])?       $btn_css['border-color']:$globalCss['base_color3']),
    'border-radius' =>      (isset($btn_css['border-radius'])?      $btn_css['border-radius']:5),
    'color' =>              (isset($btn_css['color'])?              $btn_css['color']:'#FFFFFF'),
    'width' =>              (isset($btn_css['width'])?              $btn_css['width']:'150px'),
    'height' =>             (isset($btn_css['height'])?             $btn_css['height']:'36px'),
    'line-height' =>        (isset($btn_css['line-height'])?        $btn_css['line-height']:'36px')
);
?>
<!--[if mso]>
<v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word"
             href="<?= $content['href']; ?>" style="v-text-anchor:middle; height:<?= $cfg['height']; ?>; width:<?= $cfg['width']; ?>;"
             arcsize="<?= $cfg['border-radius']+2; ?>%" strokecolor="<?= $cfg['border-color']; ?>" fillcolor="<?= $cfg['background-color']; ?>">
    <w:anchorlock/>
    <center class="btn_element btn_rounded_5" style="color:<?= $cfg['color']; ?>;"><?= $content['text']; /*&rarr;*/ ?></center>
</v:roundrect>
<![endif]-->
<a href="<?= $content['href']; ?>" class="btn_element btn_rounded_5" style="background-color:<?= $cfg['background-color'];
?>; border:1px solid <?= $cfg['border-color']; ?>; width:<?= $cfg['width'];
?>; line-height:<?= $cfg['height']; ?>; border-radius:<?= $cfg['border-radius']; ?>px; color:<?= $cfg['color'];
?>; display:inline-block;text-align:center; text-decoration:none; -webkit-text-size-adjust:none; mso-hide:all;"><?= $content['text']; /*&rarr;*/ ?></a>