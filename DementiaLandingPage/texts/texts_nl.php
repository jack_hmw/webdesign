<?php

return array(

    'quote1_t1' => 'Wij willen u de handvatten bieden om zo goed',
    'quote1_t2' => 'mogelijk om te gaan met dementie',

    'directbst1_title' => 'Een duidelijk overzicht met hulpmiddelen die u kunnen helpen',
    'directbst1_text' => 'Het is moeilijk om te bepalen of een persoon lijdt aan dementie. Vaak zijn er subtiele terugkerende problemen, die opvallen bij de partner of het familielid. Denk hierbij aan geheugenklachten, gedragsproblemen en veranderingen in het karakter.  Dit maakt het moeilijk om te bepalen of iemand dementie heeft en welke vorm.<br />&nbsp;<br />Daarbij is het een uitdaging om hiermee om te gaan. Dit geldt voor zowel de patient als de naasten in de omgeving. Bij Hulpmiddelwereld hebben we hier regelmatig mee te maken en begrijpen hoe lastig dit kan zijn. Met deze wetenschap hebben we een selectie producten gemaakt, die bijdragen aan  een comfortabel leven met dementie.',

    'welkefnct_title' => 'Welke oplossingen hebben wij voor u',
    'welkefnct_moreinfo' => 'Meer Informatie',

    'welkefnct_txt_10' => 'BBrain Slimme Kalenderklok',
    'welkefnct_txt_11' => 'Deze klok is ontwikkelt voor mensen die geheugenproblemen en een verminderd tijdsbesef hebben. De klok biedt de gebruiker meer structuur en zelfredzaamheid<br/>&nbsp;<br/>Bekijk hieronder de video',

    'welkefnct_txt_20' => 'Dementie muziekspeler',
    'welkefnct_txt_21' => 'Iedereen moet kunnen genieten van hun favoriete muziek, ook mensen met dementie. Deze muziekspeler is speciaal ontwikkeld voor dementerenden en zo ontworpen dat het door de demente oudere zelf bediend kan worden.',

    'welkefnct_txt_30' => 'Senioren telefoons',
    'welkefnct_txt_31' => 'Grote knoppen met afbeeldingen erbij. De gebruiker hoeft alleen op een foto te klikken om verbinding te maken. Voor beginnend dementerenden is dit een prettige oplossing.',

    'welkefnct_txt_40' => 'Aangepast bestek',
    'welkefnct_txt_41' => 'Heeft u moeite met het eten vanwege bijvoorbeeld reuma, beperkte grip of een verminderde handfunctie? U kunt dan gebruik maken van aangepast bestek. Aangepast bestek is verkrijgbaar in vele uitvoeringen.',

    'welkefnct_txt_50' => 'Pillendoosje met alarmering',
    'welkefnct_txt_51' => 'Dient u dagelijks of meerdere keren per dag medicijnen te nemen? Dit is niet voor iedereen altijd even eenvoudig. Onthouden om de juiste medicijnen te nemen en in de juiste hoeveelheid gaat niet altijd gaat niet altijd zoals verwacht. ',

    'welkefnct_txt_60' => 'Kalender/dementie klokken',
    'welkefnct_txt_61' => 'Dient u dagelijks of meerdere keren per dag medicijnen te nemen? Dit is niet voor iedereen altijd even eenvoudig. Onthouden om de juiste medicijnen te nemen en in de juiste hoeveelheid gaat niet altijd gaat niet altijd zoals verwacht.',





    'instrvideo_godw1' => 'Bekijk hieronder de video over de BBrain<br/>&nbsp;<br/>De beste oplossing voor mensen met beginnende dementie',
    'instrvideo_title' => 'Bekijk hoe de BBrain Slimme Kalenderklok werkt',
    'instrvideo_t01' => 'Kijk de video en zie wat de mogelijkheden zijn!',
    'instrvideo_t02' => 'Klik op de pijl om de mogelijkheden per functie uitgelegd te krijgen.',

    'reviews_title' => 'Wat vinden de kopers',
    'reviews_r11' => 'Janine',
    'reviews_r12' => 'Heeft de klok aangeschaft voor haar moeder',
    'reviews_r13' => 'Ik zag de BBrain langskomen en het product leek me direct geschikt voor mijn moeder.
De laatste weken is haar tijdsbesef achteruit aan het gaan en ze vergeet kleine dingen.
De agendafunctie en het duidelijk weergeven van de tijd helpt daarbij.
Een heel fijn product!!',
    'reviews_r21' => 'Robert',
    'reviews_r22' => 'Heeft de klok voor zijn vader gekocht',
    'reviews_r23' => 'Mijn vader is beginnend dementerend. Na het slapen is hij altijd erg verward.
Het dementiethema van de klok is voor hem heel prettig en een goede houvast.
De klok maakt hem een stuk zelfverzekerder.',
    'reviews_r31' => 'Yvonne',
    'reviews_r32' => 'Heeft de klok aangeschaft voor haar gezin',
    'reviews_r33' => 'In een gezinssamenstelling van 6 personen, is het nogal lastig om de structuur en overzicht te behouden.
Wij gebruiken de klok als een digitale agenda voor alle afspraken. Heel handig!',

    'ftbanner_title' => 'De klok die uw leven <strong>veranderd</strong>',
    'ftbanner_li1' => 'Spreekt de datum & tijd uit',
    'ftbanner_li2' => 'Stuurt herinneringen voor belangrijke afspraken',
    'ftbanner_li3' => 'Blijf in contact met uw kinderen met de berichten en foto functie',
    'ftbanner_btn' => 'Bestel de BBrain klok direct',
);


