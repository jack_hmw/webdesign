
<div class="blocks columns nopadding-lr" id="ftform">
    <div class="wp33">
        <div <?php _ibx('bclockft.jpg'); ?>></div>
    </div>
    <div class="wp66">
        <h3><?php __e('ftbanner_title'); ?></h3>
        <ul>
            <li><i class="fa fa-check"></i> <?php __e('ftbanner_li1'); ?></li>
            <li><i class="fa fa-check"></i> <?php __e('ftbanner_li2'); ?></li>
            <li><i class="fa fa-check"></i> <?php __e('ftbanner_li3'); ?></li>
        </ul>
        <a href="<?php __lk('directbst1_button'); ?>" class="button fill red button_t3" style="margin-top:1.6em;padding-left: 2em;">
            <i class="fa fa-arrow-right" style="margin-right: 16px;"></i>
            <?php __e('ftbanner_btn'); ?>
        </a>
    </div>
</div>
