
<div class="blocks nopadding" id="welke_functies">
    <h2><?php __e('welkefnct_title'); ?></h2>
    <div class="separator_h5" style="width:40%;margin-left:-16px;"></div>

    <div class="blocks columns">
        <div class="w33">
            <?php _itg('wkfct1.jpg'); ?>
            <div class="bclck">
                <h4><?php __e('welkefnct_txt_10'); ?></h4>
                <p><?php __e('welkefnct_txt_11'); ?></p>
                <p class="btnct">
                    <a href="<?php __lk('welkefnct_link_11'); ?>" class="button button_t3"><?php __e('welkefnct_moreinfo'); ?></a>
                </p>
            </div>
        </div>
        <div class="w33">
            <?php _itg('wkfct2.jpg'); ?>
            <div class="bclck">
                <h4><?php __e('welkefnct_txt_20'); ?></h4>
                <p><?php __e('welkefnct_txt_21'); ?></p>
                <p class="btnct">
                    <a href="<?php __lk('welkefnct_link_21'); ?>" class="button button_t3"><?php __e('welkefnct_moreinfo'); ?></a>
                </p>
            </div>
        </div>
        <div class="w33">
            <?php _itg('wkfct3.jpg'); ?>
            <div class="bclck">
                <h4><?php __e('welkefnct_txt_30'); ?></h4>
                <p><?php __e('welkefnct_txt_31'); ?></p>
                <p class="btnct">
                    <a href="<?php __lk('welkefnct_link_31'); ?>" class="button button_t3"><?php __e('welkefnct_moreinfo'); ?></a>
                </p>
            </div>
        </div>
        <div class="w33">
            <?php _itg('wkfct4.png'); ?>
            <div class="bclck">
                <h4><?php __e('welkefnct_txt_40'); ?></h4>
                <p><?php __e('welkefnct_txt_41'); ?></p>
                <p class="btnct">
                    <a href="<?php __lk('welkefnct_link_41'); ?>" class="button button_t3"><?php __e('welkefnct_moreinfo'); ?></a>
                </p>
            </div>
        </div>
        <div class="w33">
            <?php _itg('wkfct5.jpg'); ?>
            <div class="bclck">
                <h4><?php __e('welkefnct_txt_50'); ?></h4>
                <p><?php __e('welkefnct_txt_51'); ?></p>
                <p class="btnct">
                    <a href="<?php __lk('welkefnct_link_51'); ?>" class="button button_t3"><?php __e('welkefnct_moreinfo'); ?></a>
                </p>
            </div>
        </div>
        <div class="w33">
            <?php _itg('wkfct6.jpg'); ?>
            <div class="bclck">
                <h4><?php __e('welkefnct_txt_60'); ?></h4>
                <p><?php __e('welkefnct_txt_61'); ?></p>
                <p class="btnct">
                    <a href="<?php __lk('welkefnct_link_61'); ?>" class="button button_t3"><?php __e('welkefnct_moreinfo'); ?></a>
                </p>
            </div>
        </div>
    </div>
</div>