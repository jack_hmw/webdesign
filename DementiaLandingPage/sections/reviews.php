
<div class="blocks nopadding-lr" id="reviews">

    <h2><?php __e('reviews_title'); ?></h2>
    <div class="separator_h5" style="width:40%;margin:0 auto;"></div>

    <div class="blocks columns nopadding-lr">
        <div class="w33">
            <div class="revface">
                <div class="greyround">
                    <div <?php _ibx('Janine_bblp092018.png'); ?>></div>
                </div>
            </div>
            <div class="revtext">
                <h4><?php __e('reviews_r11'); ?></h4>
                <p class="italic"><?php __e('reviews_r12'); ?></p>
                <p class="stars">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </p>
                <p class="txt"><?php __e('reviews_r13'); ?></p>
            </div>
        </div>
        <div class="w33">
            <div class="revface">
                <div class="greyround">
                    <div <?php _ibx('Robert_bblp092018.png'); ?>></div>
                </div>
            </div>
            <div class="revtext">
                <h4><?php __e('reviews_r21'); ?></h4>
                <p class="italic"><?php __e('reviews_r22'); ?></p>
                <p class="stars">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-half"></i>
                </p>
                <p class="txt"><?php __e('reviews_r23'); ?></p>
            </div>
        </div>
        <div class="w33">
            <div class="revface">
                <div class="greyround">
                    <div <?php _ibx('Yvonne_bblp092018.png'); ?>></div>
                </div>
            </div>
            <div class="revtext">
                <h4><?php __e('reviews_r31'); ?></h4>
                <p class="italic"><?php __e('reviews_r32'); ?></p>
                <p class="stars">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </p>
                <p class="txt"><?php __e('reviews_r33'); ?></p>
            </div>
        </div>
    </div>
</div>